<?php
class Model_kueri extends CI_Model{
    function __construct(){
        parent::__construct();
		
    }
	public function get_array($table)
    {
        $this->db->select('*');
        $this->db->from($table);
		$query = $this->db->get();
		return $query->result();
    }
	
	public function pilih_array($table,$where)
    {
        $this->db->select('*');
        $this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
    }
	
	public function pilih_array_order($table,$where,$order,$orderby)
    {
        $this->db->select('*');
        $this->db->from($table);
		$this->db->where($where);
		$this->db->order_by($order,$orderby);
		$query = $this->db->get();
		return $query->result();
    }
	
	public function pilih_array_order_limit($table,$where,$order,$orderby,$limit,$start)
    {
        $this->db->select('*');
        $this->db->from($table);
		$this->db->where($where);
		 $this->db->limit($limit, $start);
		$this->db->order_by($order,$orderby);
		$query = $this->db->get();
		return $query->result();
    }
	
	public function return_kolom_array($kolom,$tabel,$where)
	{
		$this->db->select($kolom);
		$this->db->from($tabel);
		$this->db->where($where);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
			 return  $row->$kolom;	
			}
			
		} else { return ''; } 
		
		
	}
	
	public function cek_jumlah_array($tabel,$where)
	{
		$this->db->select('*');
        $this->db->from($tabel);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->num_rows();
		
	}
	
	public function cek_jumlah_array_minimal($kolom,$tabel,$where)
	{
		$this->db->select($kolom);
        $this->db->from($tabel);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->num_rows();
		
	}
	
	
	
	public function kueri_row_array($tabel,$where)
	{
		$this->db->select('*');
        $this->db->from($tabel);
		$this->db->where($where);
		$query = $this->db->get();
		if ($query->num_rows() > 0) { return $query->row(); } else { return FALSE; }
		
	}
	
	
	public function return_sum($kolom,$tabel,$where,$by,$where2,$by2)
	{
		$this->db->select_sum($kolom);
        $this->db->from($tabel);
		$this->db->where($where,$by);
		$this->db->where($where2,$by2);
		$query = $this->db->get();
		foreach ($query->result() as $row)
		{
		 $jum = $row->$kolom;
		
		return $jum;
		}
		
	}
	
	public function return_kolom($kolom,$tabel,$kolomwhere,$where)
	{
		$this->db->select($kolom);
		$this->db->from($tabel);
		$this->db->where($kolomwhere,$where);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
			 return  $row->$kolom;	
			}
			
		} else { return ' '; } 
		
		
	}
	
	
	
	public function total_sum($kolom,$tabel,$where)
	{
		$this->db->select_sum($kolom);
        $this->db->from($tabel);
		$this->db->where($where);
		$query = $this->db->get();
		foreach ($query->result() as $row)
		{
		 $jum = $row->$kolom;
		}
		return $jum;
	}
	
	public function total_tabel($kolom,$tabel,$where)
	{
		$this->db->select($kolom);
        $this->db->from($tabel);
		$this->db->where($where);
		return $this->db->count_all_results();
	}
	
	public function total_tabel_rows($kolom,$tabel,$where)
	{
		$this->db->select($kolom)->from($tabel)->where($where); 
		$q = $this->db->get(); 
		return $q->num_rows();
	}

	public function hasil_tes($idpeserta,$idujian)
	{
		$hasiltes = 'MS';
		$cekada = $this->db->query("SELECT id_peserta_hasil FROM peserta_hasil WHERE peserta_id = '$idpeserta' AND ujian_id = '$idujian'")->num_rows();
		if($cekada < 1) { $hasiltes = 'TMS'; }
		$ceksyarat1 = $this->db->query("SELECT id_peserta_hasil FROM peserta_hasil WHERE peserta_id = '$idpeserta' AND ujian_id = '$idujian' AND jumlah_benar < 10")->num_rows();
        if($ceksyarat1 > 0) { $hasiltes = 'TMS'; }
		$jawaban_peserta = $this->pilih_array('peserta_hasil',array('peserta_id'=>$idpeserta,'ujian_id'=>$idujian)); 
        $selisih = '';
        $satu = 0;
        
        foreach ($jawaban_peserta as $jwb)
        {
            $dua = $jwb->jumlah_benar;
            $satu = ($satu == 0) ? $dua : $satu;
            $dikurang = $dua - $satu;
            if(abs($dikurang) > 3) { $hasiltes = 'TMS'; }
            $selisih .= 'satu '.$satu.' dua '.$dua.' dikurang '.$dikurang.',<br>';   
            $satu =  $jwb->jumlah_benar;
		}
		
		return $hasiltes;
	}

	

	

	

	

	
	
	
	
	
	
	
	
	
	
	
	
	
}