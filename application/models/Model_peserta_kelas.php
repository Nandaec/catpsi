<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_peserta_kelas extends CI_Model {

    var $table = 'peserta';
    var $table2 = 'users';
	var $column_order = array('users.username','users.first_name',NULL);
	var $column_search = array('users.username','users.first_name'); 
	var $order = array('users.username','asc'); 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query($idkelas)
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('kelas_id',$idkelas);
        $this->db->join($this->table2,$this->table.'.peserta_id = '.$this->table2.'.id','LEFT');
        $i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}
		
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
		
		
	}

	function get_datatables($idkelas)
	{
		$this->_get_datatables_query($idkelas);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($idkelas)
	{
		$this->_get_datatables_query($idkelas);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($idkelas)
	{
		$this->db->select('id_peserta');
		$this->db->from($this->table);
		$this->db->where('kelas_id',$idkelas);
        return $this->db->count_all_results();
    }
    
    public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	


}
