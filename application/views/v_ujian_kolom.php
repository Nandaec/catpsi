<div class="page-content">
    <div class="page-header">
        <h1><?=$judul;?></h1>
        
    </div><!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12">
        
        <div class="widget-box" id="widget-box-1">
            <div class="widget-header">
                <h5 class="widget-title"><?=$judul;?></h5>

            </div>

            <div class="widget-body">
            <div class="row">
				<div class="col-xs-12">
                <br>
                    <form class="form-horizontal" role="form" id="formkolom">
                        <input type="hidden" name="idujian" value="<?=$idujian;?>">
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Kolom </label>
                            <div class="col-sm-8">
                                <select name="kolom" id="kolom" class="form-control">
                                <?php for ($i=1;$i<=$jumkolom;$i++) {
                                 echo "<option value='".$i."'>Kolom ".$i."</option>";
                                } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Huruf </label>
                            <div class="col-sm-8">
                                <select name="huruf" id="huruf" class="form-control">
                                <?php 
                                $huruf = array('A','B','C','D','E');
                                $jh = count($huruf);
                                for ($a=0;$a<$jh;$a++) {
                                 echo "<option value='".$huruf[$a]."'>".$huruf[$a]."</option>";
                                } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Nilai </label>

                            <div class="col-sm-8">
                                <input type="text" id="form-field-1" placeholder="nilai" name="nilai" class="col-xs-10 col-sm-5" maxlength="1" />
                            </div>
                        </div>
                        
                    </form>
                    <div class="clearfix form-actions">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn btn-info" type="button" id="btnSave" onclick="savekolom()">
                                Tambahkan
                            </button>
                        </div>
                    </div>

                </div>
                <div id="loading" class="text-center"></div>
                <div class="col-xs-12">
                <div id="hasilkolom"></div>
                </div>
            </div>      

            
                
            </div>
        </div>
            
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.page-content -->
<script type="text/javascript">
function savekolom()
{
    validasikolom();
  
    url = "<?php echo site_url('admin/kolom_add')?>";
    
    $('#btnSave').text('saving...'); 
    $('#btnSave').attr('disabled',true); 
        
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#formkolom').serialize(),
        dataType: "JSON",
        success: function(hasil)
        {
            console.log(hasil);
            if(hasil.status == 'berhasil') 
            {  hasil_kolom();
               
            } else {
                alert('gagal menyimpan data kolom')
            }

            $('#btnSave').text('Tambahkan'); 
            $('#btnSave').attr('disabled',false); 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Eror Add/Update data');
            $('#btnSave').text('Tambahkan'); 
            $('#btnSave').attr('disabled',false); 

        }
    });
}
function validasikolom()
{
  
  if ( $('#nilai').val() == '') { alert('Nilai harus diisi'); return FALSE; }
  
}

function hasil_kolom()
{
   var idujian = <?=$idujian;?>;
   var urlhasil =  "<?php echo site_url('admin/hasil_kolom')?>";
   $("#hasilkolom").load(urlhasil,{idujian : idujian},function(){
    });

}
$( document ).ready(function() {
    hasil_kolom();
});

function hapus_kolom(id)
{
    $.ajax({
        url : "<?php echo site_url('admin/kolom_delete')?>",
        type: "POST",
        data: "idkolom="+id,
        dataType: "JSON",
        success: function(data)
        {
            if(data.status == 'berhasil') 
            {
                hasil_kolom();
            }
        }
    });
}     

function generate_soal(kolom)
{
    var idujian = <?=$idujian;?>;
    var jumlahsoal = $('#jumlahsoal').val();
    $("#loading").show().html("<h4>Please wait... membuat soal ujian</h4><br /><br /><br /><img src='<?=base_url('assets/images/loading.gif');?>' height='auto'>");  
    $.ajax({
        url : "<?php echo site_url('admin/generate_soal')?>",
        type: "POST",
        data: "idujian="+idujian+"&jumlahsoal="+jumlahsoal+"&kolom="+kolom,
        dataType: "JSON",
		success: function(data)
        {   console.log(data);
		    if(data.status == 'berhasil') 
            {
                $("#loading").hide();
            }
		
        }
    });
}

function lihat_soal(idujian,kolom)
{
    $.ajax({
        url : "<?php echo site_url('admin/lihat_soal')?>/" + idujian + "/"+kolom,
        type: "GET",
        success: function(data)
        {
            $('.modal-body').html(data);
            $('#modal_form').modal('show');
            $('.modal-title').text('Lihat Soal'); 

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Gagal mengambil data');
        }    
    });
}
 </script>

<div class="modal inmodal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
           <h5 class="modal-title">Soal Ujian</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
            <div class="modal-body">
                   
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
            </div>
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
 