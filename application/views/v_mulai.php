<input type="hidden" id="idujian" value="">
<input type="hidden" id="kolomke" value="<?=$_SESSION['soalke'];?>">
<input type="hidden" id="makskolom" value="<?=$makskolom;?>">
<input type="hidden" id="jenis" value="<?=$jenis;?>">
<div class="page-content">
<div class="page-header">
        <h1><?=$judul;?></h1>
        
    </div><!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12">
        
        <div class="widget-box" id="widget-box-1">
            <div class="widget-body">
            <div id="datahtml"></div>
            </div>
        </div>
            
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.page-content -->

<script>
$( document ).ready(function() {
    $('#sidebar').css('visibility', 'hidden');
    let jenis = $(`#jenis`).val()
    if(jenis == 'koran'){
        get_soal_koran();
    }else{
        get_soal();
    }
});

function get_soal_koran()
{   var url = "<?=site_url('peserta/get_soal_koran');?>";
    var kolom = $('#kolomke').val();
    //var kolom = <?=$_SESSION['soalke'];?>;
    var makskolom = $('#makskolom').val();
    if(parseInt(kolom) > parseInt(makskolom))
    {
        window.location.replace("<?=site_url('peserta/selesai');?>");
    } else {
    $('#datahtml').load( url, { kolom : kolom}, function(){ })
    }
}

function get_soal()
{   var url = "<?=site_url('peserta/get_soal');?>";
    var kolom = $('#kolomke').val();
    //var kolom = <?=$_SESSION['soalke'];?>;
    var makskolom = $('#makskolom').val();
    if(parseInt(kolom) > parseInt(makskolom))
    {
        window.location.replace("<?=site_url('peserta/selesai');?>");
    } else {
    $('#datahtml').load( url, { kolom : kolom}, function(){ })
    }
}


</script>