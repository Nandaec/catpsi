<div class="page-content">
    <div class="page-header">
        <h1><?=$judul;?></h1>
        
    </div><!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12">
        
        <div class="widget-box" id="widget-box-1">
            <div class="widget-header">
                <h5 class="widget-title"><?=$judul;?></h5>

                <div class="widget-toolbar no-border">
                <button class="btn btn-sm btn-primary pull-right" id="btnSave" type="button" onclick="save()">Tambah Peserta</button>

                    
                </div>
            </div>
            <div class="widget-body">
            <input type="hidden" value="<?=$idkelas;?>" id="idkelas" name="idkelas"/>    
            <div class="widget-main"> <div>
                <label for="form-field-select-4">Cari User/Peserta</label>
                <select multiple="" class="chosen-select form-control tag-input-style" id="form-field-select-4" data-placeholder="Pilih semua peserta kelas ini...">
                </select>
               </div>
            </div>   
            <div class="widget-body">
                <div class="widget-main">
                <table id="tbl_data" class="table table-striped table-bordered table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th>Username</th>
                            <th>Nama Peserta</th>
                            <th width="10%"></th>
                        </tr>
                    </thead>    
                    <tbody>
                    </tbody>    
                    </table>
                </div>
            </div>
        </div>
            
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.page-content -->
<script type="text/javascript">
   var table;
   var save_method; 
   $(document).ready(function() {
            table = $('#tbl_data').DataTable({ 
               
              "processing": true, //Feature control the processing indicator.
              "serverSide": true, //Feature control DataTables' server-side processing mode.
              "order": [], //Initial no order.
          
              // Load data for the table's content from an Ajax source
              "ajax": {
                "url": "<?php echo site_url('admin/peserta_kelas_list/'.$idkelas)?>",
                "type": "POST"
              },
          
              //Set column definition initialisation properties.
              "columnDefs": [
              { 
                "targets": [ 0 ], //last column
                "orderable": false, //set not orderable
              },
              ]
          
            });
    get_peserta();
   });

function reload_table()
{
    table.ajax.reload(null,false); 
    
}            
  function tambah()
{
    save_method = 'add';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error'); 
    $('.help-block').empty();
    get_peserta();
    $('#modal_form').modal('show'); 
    $('.modal-title').text('Tambah Peserta'); 
}

function save()
{
    var allpeserta = $('#form-field-select-4').val();
    var id_kelas = <?=$idkelas;?>;
    url = "<?php echo site_url('admin/peserta_kelas_add')?>";
    
    $('#btnSave').text('saving...'); 
    $('#btnSave').attr('disabled',true); 
        
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: {idkelas : id_kelas, allpeserta : allpeserta},
        dataType: "JSON",
        success: function(hasil)
        {
            console.log(hasil);
            if(hasil.status == 'berhasil') 
            {
                $('#modal_form').modal('hide');
                get_peserta();
                reload_table();

            }

            $('#btnSave').text('Tambah Peserta'); 
            $('#btnSave').attr('disabled',false); 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            //alert('Eror Add/Update data');
            $('#btnSave').text('Tambah Peserta'); 
            $('#btnSave').attr('disabled',false); 

        }
    });
    
}



function konfirm_hapus(id)
{
  $('[name="iddelete"]').val(id);
  $('.modal-title').text('Hapus Data');
  $('#modal_hapus').modal('show');

}

function hapus()
{
    var url = "<?php echo site_url('admin/peserta_kelas_delete')?>";
    $('#btnDelete').text('Menghapus...'); 
    $('#btnDelete').attr('disabled',true); 
    
    $.ajax({
        url : url,
        type: "POST",
        data: $('#formdelete').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            console.log(data);
            if(data.status == 'berhasil') 
            {
                $('#modal_hapus').modal('hide');
                reload_table();
                get_peserta();

            }

            $('#btnDelete').text('Hapus'); 
            $('#btnDelete').attr('disabled',false); 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            salah('Data telah digunakan sistem');
            $('#btnDelete').text('Hapus'); 
            $('#btnDelete').attr('disabled',false); 

        }
    });
} 

function validasi()
{
  
  if ( $('#peserta').val() == '') { alert('Peserta harus diisi'); return FALSE; }
  
}
     
 </script>
 

<div class="modal  fade" id="modal_hapus">
          <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-danger">
            <h4 class="modal-title">Hapus Data</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
            <p>Konfirmasi Penghapusan Data</p>
            <form action="#" id="formdelete">
            <input type="hidden" name="iddelete" value="">  
            </form> 
            </div>
            <div class="modal-footer">
            <button type="button" id="btnDelete" class="btn btn-danger float-right" onclick="hapus()">Hapus</button>
            <button type="button" class="btn btn-info" data-dismiss="modal">Tutup</button>
            </div>
          </div>
          <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
</div>
<script>
function get_peserta()
{
    $.ajax({
        type	: 'POST',
        url		: "<?= site_url('admin/get_peserta_kelas/'.$idkelas);?>",
        cache	: false,
        success	: function(result){
            $('#form-field-select-4').html(result);
            $("#form-field-select-4").trigger("chosen:updated");
            
        }
    });
}   
</script>