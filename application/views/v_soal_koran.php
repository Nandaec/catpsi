<div class="row" id="tabelkolom" style="background-color:#FFF;">
    <!-- <div class="col-xs-6"> </div> -->
    <form id="formjawab" method="post">
        <input type="hidden" id="jumsoal" name="jumsoal" value="<?=$jumsoal;?>">
        <input type="hidden" id="id_ujian" name="id_ujian" value="<?=$idujian;?>">
        <input type="hidden" id="kolomujian" name="kolomujian" value="<?=$kolom;?>">
        <input type="hidden" id="tot_baris" value="<?=$s->baris;?>">
        <?php foreach ($soals->result() as $sl) { ?>
        <input type="hidden" id="soalke<?=$sl->kolom;?>" name="soalke<?=$sl->kolom;?>" value="">
        <input type="hidden" id="jawabke<?=$sl->kolom;?>" name="jawabke<?=$sl->kolom;?>" value="">
        <?php } ?>

    <div class="col-xs-6">
        <div class="table-responsive scroll" style="height: 450px;">
            <table id="simple-table" class="table-sm table-bordered">
                <thead>
                    <tr>
                        <th class="center w-15">Nomor</th>
                        <th class="center w-15">Soal</th>
                        <th class="center w-15">Jawaban</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $no = 1;?>
                    <?php for ($i=1;$i<=$jumsoal;$i++) { ?>
                    <tr>
                        <td class="center">20</td>
                        <td>
                            <input type="text" class="form-control center pointer" value="<?=$s->s20;?>" disabled>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control text-center" name="inp_jawab[]" id="inp_jawab<?='20';?>" onkeypress="return isNumber(event)" onkeyup="set_jawaban(20)" style="background-color: #ccfcd9;" autocomplete="off">
                        </td>
                    </tr>

                    <tr>
                        <td class="center">19</td>
                        <td>
                            <input type="text" class="form-control center pointer" value="<?=$s->s19;?>" disabled>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control text-center" name="inp_jawab[]" id="inp_jawab<?='19';?>" onkeypress="return isNumber(event)" onkeyup="set_jawaban(19)" style="background-color: #ccfcd9;" autocomplete="off">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="center">18</td>
                        <td>
                            <input type="text" class="form-control center pointer" value="<?=$s->s18;?>" disabled>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control text-center" name="inp_jawab[]" id="inp_jawab<?='18';?>" onkeypress="return isNumber(event)" onkeyup="set_jawaban(18)" style="background-color: #ccfcd9;" autocomplete="off">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="center">17</td>
                        <td>
                            <input type="text" class="form-control center pointer" value="<?=$s->s17;?>" disabled>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control text-center" name="inp_jawab[]" id="inp_jawab<?='17';?>" onkeypress="return isNumber(event)" onkeyup="set_jawaban(17)" style="background-color: #ccfcd9;" autocomplete="off">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="center">16</td>
                        <td>
                            <input type="text" class="form-control center pointer" value="<?=$s->s16;?>" disabled>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control text-center" name="inp_jawab[]" id="inp_jawab<?='16';?>" onkeypress="return isNumber(event)" onkeyup="set_jawaban(16)" style="background-color: #ccfcd9;" autocomplete="off">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="center">15</td>
                        <td>
                            <input type="text" class="form-control center pointer" value="<?=$s->s15;?>" disabled>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control text-center" name="inp_jawab[]" id="inp_jawab<?='15';?>" onkeypress="return isNumber(event)" onkeyup="set_jawaban(15)" style="background-color: #ccfcd9;" autocomplete="off">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="center">14</td>
                        <td>
                            <input type="text" class="form-control center pointer" value="<?=$s->s14;?>" disabled>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control text-center" name="inp_jawab[]" id="inp_jawab<?='14';?>" onkeypress="return isNumber(event)" onkeyup="set_jawaban(14)" style="background-color: #ccfcd9;" autocomplete="off">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="center">13</td>
                        <td>
                            <input type="text" class="form-control center pointer" value="<?=$s->s13;?>" disabled>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control text-center" name="inp_jawab[]" id="inp_jawab<?='13';?>" onkeypress="return isNumber(event)" onkeyup="set_jawaban(13)" style="background-color: #ccfcd9;" autocomplete="off">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="center">12</td>
                        <td>
                            <input type="text" class="form-control center pointer" value="<?=$s->s12;?>" disabled>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control text-center" name="inp_jawab[]" id="inp_jawab<?='12';?>" onkeypress="return isNumber(event)" onkeyup="set_jawaban(12)" style="background-color: #ccfcd9;" autocomplete="off">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="center">11</td>
                        <td>
                            <input type="text" class="form-control center pointer" value="<?=$s->s11;?>" disabled>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control text-center" name="inp_jawab[]" id="inp_jawab<?='11';?>" onkeypress="return isNumber(event)" onkeyup="set_jawaban(11)" style="background-color: #ccfcd9;" autocomplete="off">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="center">10</td>
                        <td>
                            <input type="text" class="form-control center pointer" value="<?=$s->s10;?>" disabled>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control text-center" name="inp_jawab[]" id="inp_jawab<?='10';?>" onkeypress="return isNumber(event)" onkeyup="set_jawaban(10)" style="background-color: #ccfcd9;" autocomplete="off">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="center">9</td>
                        <td>
                            <input type="text" class="form-control center pointer" value="<?=$s->s9;?>" disabled>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control text-center" name="inp_jawab[]" id="inp_jawab<?='9';?>" onkeypress="return isNumber(event)" onkeyup="set_jawaban(9)" style="background-color: #ccfcd9;" autocomplete="off">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="center">8</td>
                        <td>
                            <input type="text" class="form-control center pointer" value="<?=$s->s8;?>" disabled>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control text-center" name="inp_jawab[]" id="inp_jawab<?='8';?>" onkeypress="return isNumber(event)" onkeyup="set_jawaban(8)" style="background-color: #ccfcd9;" autocomplete="off">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="center">7</td>
                        <td>
                            <input type="text" class="form-control center pointer" value="<?=$s->s7;?>" disabled>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control text-center" name="inp_jawab[]" id="inp_jawab<?='7';?>" onkeypress="return isNumber(event)" onkeyup="set_jawaban(7)" style="background-color: #ccfcd9;" autocomplete="off">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="center">6</td>
                        <td>
                            <input type="text" class="form-control center pointer" value="<?=$s->s6;?>" disabled>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control text-center" name="inp_jawab[]" id="inp_jawab<?='6';?>" onkeypress="return isNumber(event)" onkeyup="set_jawaban(6)" style="background-color: #ccfcd9;" autocomplete="off">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="center">5</td>
                        <td>
                            <input type="text" class="form-control center pointer" value="<?=$s->s5;?>" disabled>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control text-center" name="inp_jawab[]" id="inp_jawab<?='5';?>" onkeypress="return isNumber(event)" onkeyup="set_jawaban(5)" style="background-color: #ccfcd9;" autocomplete="off">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="center">4</td>
                        <td>
                            <input type="text" class="form-control center pointer" value="<?=$s->s4;?>" disabled>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control text-center" name="inp_jawab[]" id="inp_jawab<?='4';?>" onkeypress="return isNumber(event)" onkeyup="set_jawaban(4)" style="background-color: #ccfcd9;" autocomplete="off">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="center">3</td>
                        <td>
                            <input type="text" class="form-control center pointer" value="<?=$s->s3;?>" disabled>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control text-center" name="inp_jawab[]" id="inp_jawab<?='3';?>" onkeypress="return isNumber(event)" onkeyup="set_jawaban(3)" style="background-color: #ccfcd9;" autocomplete="off">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="center">2</td>
                        <td>
                            <input type="text" class="form-control center pointer" value="<?=$s->s2;?>" disabled>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control text-center" name="inp_jawab[]" id="inp_jawab<?='2';?>" onkeypress="return isNumber(event)" onkeyup="set_jawaban(2)" style="background-color: #ccfcd9;" autocomplete="off">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="center">1</td>
                        <td>
                            <input type="text" class="form-control center pointer" value="<?=$s->s1;?>" disabled>
                        </td>
                        <td>
                            <input type="hidden" class="form-control text-center" name="inp_jawab[]" id="inp_jawab<?='1';?>" onkeypress="return isNumber(event)" onkeyup="set_jawaban(1)" style="background-color: #ccfcd9;" autocomplete="off">
                        </td>
                    </tr>
                    <!-- <tr id="tabel_jawaban"></tr> -->
                    <?php $no++; } ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-xs-6">
        <table id="simple-table" class="table table-bordered table-hover">
        <tr>
        <td class="text-center" style="line-height: 30px;font-size:16px;">
            <b>
                <?=$this->ion_auth->user()->row()->first_name;?>
                Kolom <?=$kolom;?>
            </b>
        </td>
        </tr>
        <tr>
        <td class="text-center" style="line-height: 30px;font-size:16px;">SISA WAKTU</td>
        </tr>
        <tr>
        <td class="text-center" style="line-height: 30px;font-size:16px;"><div id="countdown"></div></td>
        </tr>
        </table>
    </div>
</div>
</form>
<script>
$(document).ready(function() {
    var timer2 = localStorage.getItem('timer');
    if(timer2 === null) timer2 = "<?=$durasi;?>:<?=$detik;?>";
    $('#countdown').html(timer2);

    var interval = setInterval(function() {
        var timer = timer2.split(':');
        var minutes = parseInt(timer[0], 10);
        var seconds = parseInt(timer[1], 10);
        --seconds;
        minutes = (seconds < 0) ? --minutes : minutes;
        if (minutes < 0){
            clearInterval(interval);
            localStorage.removeItem('timer');
            next_kolom();
            $('html, body').animate({ scrollTop: 0 }, 'fast');
        }else{
            seconds = (seconds < 0) ? 59 : seconds;
            seconds = (seconds < 10) ? '0' + seconds : seconds;
            $('#countdown').html(minutes + ':' + seconds);
            timer2 = minutes + ':' + seconds;
            localStorage.setItem('timer',timer2);
        }
    }, 1000);

    getSoal()
});

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function getSoal(){
    var html_soal = '';
    let tot_baris = $(`#tot_baris`).val()

    html_soal += `<td class="center">Jawaban</td>`
    for (i = 0; i < tot_baris; i++) {
        if(i!=0){
            html_soal += `
                <td>
                    <input type="text" class="form-control text-center" name="inp_jawab[]" id="inp_jawab${i}" onkeypress="return isNumber(event)" onkeyup="set_jawaban(${i})" autocomplete="off">
                </td>
            `
        }else{
            html_soal += `
                <td>
                    <input type="text" class="form-control border-none bg-white" name="inp_jawab[]" id="inp_jawab${i}"  readonly>
                </td>
            `
        }
    }
    $("#tabel_jawaban").html(html_soal)
    $(`#inp_jawab${20}`).focus()
}

function set_jawaban(i){
    $(`#inp_jawab${i-1}`).focus()
    if(i>0){
        let jumsoal     = $(`#jumsoal`).val()
        let id_ujian    = $(`#id_ujian`).val()
        let kolomujian  = $(`#kolomujian`).val()
        let tot_baris   = $(`#tot_baris`).val()
        let jawaban     = $(`#inp_jawab${i}`).val() ? $(`#inp_jawab${i}`).val() : '';

        if(jawaban){
            baris = i+1
            $.ajax({
                url : '<?=site_url('peserta/simpan_jawaban_koran');?>',
                type: "POST",
                // data: { jumsoal, id_ujian, kolomujian, tot_baris, baris, jawaban },
                data: $('#formjawab').serialize(),
                dataType: "JSON",
                success: function(data)
                {
                    console.log(data)
                }
            });
        }
    }
}

function simpan()
{
    $.ajax({
        url : '<?=site_url('peserta/simpan_jawaban');?>',
        type: "POST",
        data: $('#formjawab').serialize(),
        dataType: "JSON",
        success: function(data)
        {
           
        }
    });


}

$( ".btnjawab" ).click(function() {
  var jawaban = $(this).attr("data-huruf");
  var idjawaban = $(this).attr("data-soal");
  $('#jawabpeserta'+idjawaban).html(jawaban);
  $('#soalke'+idjawaban).val(idjawaban);
  $('#jawabke'+idjawaban).val(jawaban);
  simpan();
});

function next_kolom()
{
    var kolomke = $('#kolomke').val();
    kolomnext = parseInt(kolomke)+1;
    //localStorage.setItem("kolomke", kolomnext);
    $('#kolomke').val(kolomnext);
    get_soal_koran();
}

window.onscroll = function() {myFunction()};

var navbar = document.getElementById("tabelkolom");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}


</script>        
