<div class="row">
    <div class="col-12">
        
        <div class="card">
        <div class="card-header">
        <span class="center">Poin Saya : <strong><?=$this->mylibrary->format_rupiah($this->ion_auth->user()->row()->poin_member);?></strong></span>
            <div class="card-actions">
            Poin Akan Berkurang Hanya  Ketika Pencarian Anda Sukses
            </div>
        </div>
            <div class="card-body center">
            <?php echo form_open('cari/hasil');?>    
            <div class="row mb-3">
                <div class="col-lg-2">
                    NIK Karyawan
                </div>
                <div class="col-lg-6">
                <input type="text" name="nik" class="form-control" id="nik" placeholder="Masukkan 16 Digit NIK" maxlength="16" value="<?=$nikcari;?>" autocomplete="off" required>
                </div>
                <div class="col-lg-2">
                <input name="submit" type="submit" class="btn btn-primary" value="Cari">
                </div>
            </div>
            </div>
            
            </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body center">
                <h5 class="card-title m-b-0 text-center">Hasil Pencarian : <?=$nikcari;?></h5><br />
            <table class="table table-bordered">
            <?php if ($jumlah > 0) { ?>
                <tbody>
                    <tr>
                        
                        <td width="30%">
                            Nama
                        </td>
                        <td>
                            <?=$karyawan->nama;?>
                        </td>
                    </tr>
                    <tr>
                        
                        <td>
                            NIK
                        </td>
                        <td>
                            <?=$karyawan->nik;?>
                        </td>
                    </tr>
                    <tr>
                    
                        <td>
                            Jenis Kelamin
                        </td>
                        <td>
                            <?=($karyawan->kelamin == 'L') ? 'Laki-Laki' : 'Perempuan';?>
                        </td>
                    </tr>
                    <tr>
                    
                        <td>
                            Kota
                        </td>
                        <td>
                            <?=$karyawan->kota;?>
                        </td>
                    </tr>
                    <tr>
                    
                        <td>
                            Kecamatan
                        </td>
                        <td>
                            <?=$karyawan->kecamatan;?>
                        </td>
                    </tr>
                    <tr>
                    
                        <td>
                            Tanggal Lahir (Usia)
                        </td>
                        <td>
                            <?=date('d-m-Y',strtotime($karyawan->tanggal_lahir));?> ( <?=$karyawan->usia;?> )
                        </td>
                    </tr>
                    <tr>
                    
                        <td>
                            Nama Ayah
                        </td>
                        <td>
                            <?=$karyawan->ayah;?>
                        </td>
                    </tr>
                    <tr>
                    
                        <td>
                            Nama Ibu
                        </td>
                        <td>
                            <?=$karyawan->ibu;?>
                        </td>
                    </tr>
                    <tr>
                    
                        <td>
                            Alamat
                        </td>
                        <td>
                            <?=$karyawan->alamat;?>
                        </td>
                    </tr>
                    <tr>
                    
                        <td>
                            RT/RW
                        </td>
                        <td>
                            <?=$karyawan->rtrw;?>
                        </td>
                    </tr>
                </tbody>
            <?php } else { ?>
                <tbody>
                    <tr>
                        <td class="text-center">
                            <code>Tidak ada data ditemukan</code>
                        </td>
                    </tr>
                    
                </tbody>
            <?php } ?>    
            </table>
            </div>
        </div>
    </div>
</div>