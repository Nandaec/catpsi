
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>BIMBEL PRIORITY - CAT PSIKOLOGI</title>

		<meta name="description" content="top menu &amp; navigation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<?=base_url();?>assets/font-awesome/4.5.0/css/font-awesome.min.css" />

		<!-- page specific plugin styles -->
		<link rel="stylesheet" href="<?=base_url();?>assets/css/custom.css" />
		<link rel="stylesheet" href="<?=base_url();?>assets/css/jquery-ui.custom.min.css" />
		<link rel="stylesheet" href="<?=base_url();?>assets/css/jquery.gritter.min.css" />
		<link rel="stylesheet" href="<?=base_url();?>assets/css/chosen.min.css" />
		<link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap-datepicker3.min.css" />
		<link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap-timepicker.min.css" />

		<!-- text fonts -->
		<link rel="stylesheet" href="<?=base_url();?>assets/css/fonts.googleapis.com.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="<?=base_url();?>assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="<?=base_url();?>assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
		<![endif]-->
		<link rel="stylesheet" href="<?=base_url();?>assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="<?=base_url();?>assets/css/ace-rtl.min.css" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="<?=base_url();?>assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="<?=base_url();?>assets/js/ace-extra.min.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
        <!--[if !IE]> -->
		<script src="<?=base_url();?>assets/js/jquery-2.1.4.min.js"></script>
		<!--[if lte IE 8]>
		<script src="<?=base_url();?>assets/js/html5shiv.min.js"></script>
		<script src="<?=base_url();?>assets/js/respond.min.js"></script>
		<![endif]-->
		<script src="<?=base_url();?>assets/code/highcharts.js"></script>
		<script src="<?=base_url();?>assetscode/modules/exporting.js"></script>
		<script src="<?=base_url();?>assetscode/modules/export-data.js"></script>
		<script src="<?=base_url();?>assetscode/modules/accessibility.js"></script>
		<style>
		.sticky {
		position: fixed;
		top: 0;
		width: 100%;
		}
		#tabelkolom{
         background-color:#FFF;
		 z-index:999;

		}
		</style>
	</head>

	<body class="no-skin">
		<div id="navbar" class="navbar navbar-default navbar-collapse h-navbar ace-save-state">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<div class="navbar-header pull-left">
					<a href="#" class="navbar-brand">
						<small>
							CAT PSIKOLOGI
						</small>
					</a>

					<button class="pull-right navbar-toggle navbar-toggle-img collapsed" type="button" data-toggle="collapse" data-target=".navbar-buttons,.navbar-menu">
						<span class="sr-only">Toggle user menu</span>

						<img src="<?=base_url();?>assets/images/avatars/avatar2.png" alt="User" />
					</button>

					<button class="pull-right navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#sidebar">
						<span class="sr-only">Toggle sidebar</span>

						<span class="icon-bar"></span>

						<span class="icon-bar"></span>

						<span class="icon-bar"></span>
					</button>
				</div>

				<div class="navbar-buttons navbar-header pull-right collapse navbar-collapse" role="navigation">
					<ul class="nav ace-nav">
						
						<li class="light-blue dropdown-modal user-min">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="<?=base_url();?>assets/images/avatars/avatar2.png" alt="User" />
								<span class="user-info">
									<small>Welcome,</small>
									<?=$this->ion_auth->user()->row()->first_name;?>
								</span>

								<i class="ace-icon fa fa-caret-down"></i>
							</a>

							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
							<?php
								if($this->ion_auth->in_group(1)) {
								?>
								<li>
									<a href="<?=site_url('profil');?>">
										<i class="ace-icon fa fa-user"></i>
										Profil
									</a>
								</li>
								<li class="divider"></li>
								<?php } ?>
								

								<li>
									<a href="<?=site_url('auth/logout');?>">
										<i class="ace-icon fa fa-power-off"></i>
										Logout
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>

				
			</div><!-- /.navbar-container -->
		</div>

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar h-sidebar navbar-collapse collapse ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>

				<ul class="nav nav-list">
					<li class="hover" id="menudashboard">
						<a href="<?=site_url('dashboard');?>">
							<span class="menu-text"> Dashboard </span>
						</a>
						<b class="arrow"></b>
					</li>
					<?php
					if($this->ion_auth->in_group(1)) {
					?>
					<li class="hover" id="menudashboard">
						<a href="<?=site_url('admin/user');?>">
							<span class="menu-text"> User </span>
						</a>
						<b class="arrow"></b>
					</li>
					<li class="hover" id="menudashboard">
						<a href="<?=site_url('admin/kelas');?>">
							<span class="menu-text"> Kelas </span>
						</a>
						<b class="arrow"></b>
					</li>
					<li class="hover" id="menudashboard">
						<a href="<?=site_url('admin/ujian');?>">
							<span class="menu-text"> Ujian </span>
						</a>
						<b class="arrow"></b>
					</li>
					<li class="hover" id="menudashboard">
						<a href="<?=site_url('admin/ujian_koran');?>">
							<span class="menu-text"> Tes Koran </span>
						</a>
						<b class="arrow"></b>
					</li>

					<li class="hover" id="menudashboard">
						<a href="<?=site_url('admin/home');?>">
							<span class="menu-text"> Pesan Home </span>
						</a>
						<b class="arrow"></b>
					</li>
					
                    <?php } else { 
					$idpeserta = $this->ion_auth->user()->row()->id;	
					$ceksdhujian = $this->model_kueri->cek_jumlah_array_minimal('id_peserta_ujian','peserta_ujian',array('peserta_id'=>$idpeserta,'status'=>'Y'));	
					if ($ceksdhujian > 0) {
					?>
					
					<li class="hover" id="menudashboard">
						<a href="<?=site_url('peserta/selesai');?>">
							<span class="menu-text"> Hasil Tes </span>
						</a>
						<b class="arrow"></b>
					</li>
					<?php } } ?>
					
				</ul><!-- /.nav-list -->
			</div>

			<div class="main-content">
				<div class="main-content-inner">
				<?php echo $_content; ?>
				</div>
			</div><!-- /.main-content -->

			<div class="footer">
				<div class="footer-inner">
					<div class="footer-content">
						<span class="bigger-120">
							<span class="blue bolder">Bimbel Priority</span>
							Application &copy; <?=date('Y');?>
						</span>

						&nbsp; &nbsp;
						
					</div>
				</div>
			</div>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		

		<!-- <![endif]-->

		<!--[if IE]>
<script src="<?=base_url();?>assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='<?=base_url();?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="<?=base_url();?>assets/js/bootstrap.min.js"></script>

		<!-- page specific plugin scripts -->
		<script src="<?=base_url();?>assets/js/jquery-ui.custom.min.js"></script>
		<script src="<?=base_url();?>assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="<?=base_url();?>assets/js/jquery.dataTables.min.js"></script>
		<script src="<?=base_url();?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
		<script src="<?=base_url();?>assets/js/dataTables.buttons.min.js"></script>
		<script src="<?=base_url();?>assets/js/buttons.flash.min.js"></script>
		<script src="<?=base_url();?>assets/js/buttons.html5.min.js"></script>
		<script src="<?=base_url();?>assets/js/buttons.print.min.js"></script>
		<script src="<?=base_url();?>assets/js/buttons.colVis.min.js"></script>
		<script src="<?=base_url();?>assets/js/dataTables.select.min.js"></script>
		<script src="<?=base_url();?>assets/js/jquery.gritter.min.js"></script>
		<script src="<?=base_url();?>assets/js/bootstrap-datepicker.min.js"></script>
		
		<script src="<?=base_url();?>assets/js/markdown.min.js"></script>
		<script src="<?=base_url();?>assets/js/bootstrap-markdown.min.js"></script>
		<script src="<?=base_url();?>assets/js/jquery.hotkeys.index.min.js"></script>
		<script src="<?=base_url();?>assets/js/bootstrap-wysiwyg.min.js"></script>
		<script src="<?=base_url();?>assets/js/bootbox.js"></script>
        <script src="<?=base_url();?>assets/js/chosen.jquery.min.js"></script>
		<!-- ace scripts -->
		<script src="<?=base_url();?>assets/js/ace-elements.min.js"></script>
		<script src="<?=base_url();?>assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->
		
		<script type="text/javascript">
		    
			jQuery(function($) {
				if(!ace.vars['touch']) {
					$('.chosen-select').chosen({allow_single_deselect:true}); 
					//resize the chosen on window resize
			
					$(window)
					.off('resize.chosen')
					.on('resize.chosen', function() {
						$('.chosen-select').each(function() {
							 var $this = $(this);
							 $this.next().css({'width': $this.parent().width()});
						})
					}).trigger('resize.chosen');
					//resize chosen on sidebar collapse/expand
					$(document).on('settings.ace.chosen', function(e, event_name, event_val) {
						if(event_name != 'sidebar_collapsed') return;
						$('.chosen-select').each(function() {
							 var $this = $(this);
							 $this.next().css({'width': $this.parent().width()});
						})
					});
			
			
					$('#chosen-multiple-style .btn').on('click', function(e){
						var target = $(this).find('input[type=radio]');
						var which = parseInt(target.val());
						if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
						 else $('#form-field-select-4').removeClass('tag-input-style');
					});
				}
				$('.date-picker').datepicker({
					dateFormat: 'yyyy-mm-dd',
					autoclose: true,
					todayHighlight: true
				})
			 var $sidebar = $('.sidebar').eq(0);
			 if( !$sidebar.hasClass('h-sidebar') ) return;
			
			 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
				if( event_name !== 'sidebar_fixed' ) return;
			
				var sidebar = $sidebar.get(0);
				var $window = $(window);
			
				//return if sidebar is not fixed or in mobile view mode
				var sidebar_vars = $sidebar.ace_sidebar('vars');
				if( !fixed || ( sidebar_vars['mobile_view'] || sidebar_vars['collapsible'] ) ) {
					$sidebar.removeClass('lower-highlight');
					//restore original, default marginTop
					sidebar.style.marginTop = '';
			
					$window.off('scroll.ace.top_menu')
					return;
				}
			
			
				 var done = false;
				 $window.on('scroll.ace.top_menu', function(e) {
			
					var scroll = $window.scrollTop();
					scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
					if (scroll > 17) scroll = 17;
			
			
					if (scroll > 16) {			
						if(!done) {
							$sidebar.addClass('lower-highlight');
							done = true;
						}
					}
					else {
						if(done) {
							$sidebar.removeClass('lower-highlight');
							done = false;
						}
					}
			
					sidebar.style['marginTop'] = (17-scroll)+'px';
				 }).triggerHandler('scroll.ace.top_menu');
			
			 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			
			 $(window).on('resize.ace.top_menu', function() {
				$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			 });
			
			
			});
		</script>
		
	</body>
</html>
