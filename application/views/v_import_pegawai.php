<div class="row">
    <div class="col-12">
        
        <div class="card">
        <div class="card-header">
            <?=$judul;?>
            <div class="card-actions">
            </div>
        </div>
            <div class="card-body">
            <?php echo form_open_multipart('admin/tesimport');?>    
            <div class="form-group row">
            
                <label class="col-md-2">File Upload <code class="highlighter-rouge">*Ekstensi File harus .xlsx</code></label>
                <div class="col-md-9">
                    <div class="custom-file">
                        <input type="file" name="userfile" class="custom-file-input" id="validatedCustomFile" required>
                        <label class="custom-file-label" for="validatedCustomFile">Cari File...</label>
                       
                    </div>
                </div>
            </div>
            <div class="form-group row">
            <label class="col-md-2">Format File Import</label>
                <div class="col-md-9">
                    <div class="custom-file">
                        <a href="<?php echo base_url('uploads/Formatimportpegawai.xlsx');?>" download>Download Format Sampel Import Pegawai</a>
                       
                    </div>
                </div>
            </div>    
                
            
            </div>
            <div class="border-top">
                <div class="card-body">
                    <button type="submit" class="btn btn-primary">Upload</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
