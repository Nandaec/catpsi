<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>HASIL TES</title>
    
</head>
<body class="A4 landscape" onload="window.print()">
<style>
    
@page { size:8.5in 11in; margin: 1cm }
    * {
        font-size: 12px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }

    
	@media screen, print {
    h1 {
        font-weight: bold;
        font-size: 10pt;
        text-align: center;
    }

    table { page-break-inside : avoid }
 
    table {
        border-collapse: collapse;
        width: 100%;
    }

    table .noborder{
        border : 0;
        vertical-align:top;
    }

    table .noborder td{
        vertical-align:top !important;
    }
 
    .table th {
        padding: 8px 8px;
        border:1px solid #000000;
        text-align: center;
    }
 
    .table td {
        padding: 3px 3px;
        vertical-align:top;
        border:1px solid #000000;
    }

    table td .putih{
        color: rgba(0, 0, 0, 0);
        background-color:#FFF;
    }

    .table2 th {
        
        border:1px solid #FFF;
        text-align: center;
    }
 
    .table2 td {
        
        border:1px solid #FFF;
    }

    .table2 td {
        color: rgba(0, 0, 0, 0);
        background-color:#FFF;
    }
 
    .text-center {
        text-align: center;
    }
	
    
}


</style>
    <h1>BIMBEL PRIORITY</h1>
    <h1><?=$judul;?></h1>
        <?php 
            $ujian = $this->model_kueri->kueri_row_array('ujian',array('id_ujian'=>$idujian));
            $jumlah_kolom = $ujian->jumlah_kolom;
            $kolomstart = 1;
            for ($offset=0;($offset+3)<=$jumlah_kolom;$offset++)
            {
                echo '<table cellpadding="6" class="noborder" style="border:1px solid #FFF !important;">';
                echo '<tr>';                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 echo '<tr>';
                
                for ($kolom=$kolomstart;$kolom<=($kolomstart+2);$kolom++ )
                {
                    echo '<td>';
                    $qtabel = $this->db->query("SELECT * FROM ujian_kolom WHERE ujian_id = '$idujian' AND kolom = '$kolom' ORDER BY huruf ASC");
                    if ($qtabel->num_rows() > 0)
                    {   echo '<table width="30%" class="table">';
                        echo '<tr><td colspan="5" style="text-align:center;">Kolom '.$kolom.'</td></tr>';
                        
                        echo '<tr>';
                        foreach ($qtabel->result() as $qt){
                            echo '<td style="text-align:center;"><div style="height: 17px; overflow:hidden;">'.$qt->huruf.'</div></td>';
                        }
                        echo '</tr>';
                        echo '<tr>';
                        foreach ($qtabel->result() as $qt){
                            echo '<td style="text-align:center;"><div style="height: 17px; overflow:hidden;">'.$qt->kolom_nilai.'</div></td>';
                        }
                        echo '</tr>';
                        
                        echo '</table>';
                        
                    } else {
                        echo '<table width="30%" class="table2">';
                        echo '<tr><td colspan="5">KOLOM</td></tr>';
                        echo '<tr><td>A</td><td>A</td><td>A</td><td>A</td><td>A</td></tr>';
                        echo '<tr><td>A</td><td>A</td><td>A</td><td>A</td><td>A</td></tr>';
                        echo '</table>';
                    }
                    echo '</td>';
                }
                
                echo '</tr>';      
                echo '<tr><td colspan="3" style="color: rgba(0, 0, 0, 0);">BATAS</td></tr>';                                                    
                
                // SOAL
                echo '<tr>';
                for ($kolom=$kolomstart;$kolom<=($kolomstart+2);$kolom++ )
                {
                    echo '<td style="vertical-align:top  !important;">';
                    $qsoal = $this->db->query("SELECT * FROM ujian_soal WHERE ujian_id = '$idujian' AND kolom = '$kolom' ORDER BY soal_ke ASC");
                    if ($qsoal->num_rows() > 0)
                    {   
                        $jawaban_peserta = $this->model_kueri->return_kolom_array('jawaban_peserta','peserta_hasil',array('peserta_id'=>$idpeserta,'ujian_id'=>$idujian,'kolom_soal'=>$kolom));
                        $jumlah_benar = $this->model_kueri->return_kolom_array('jumlah_benar','peserta_hasil',array('peserta_id'=>$idpeserta,'ujian_id'=>$idujian,'kolom_soal'=>$kolom));
                        $pecahjawab = explode(',',$jawaban_peserta);
                        echo '<table class="table">';
                        $no = 0;
                        echo '<tr><td colspan="7" style="text-align:center;"><div style="height: 17px; overflow:hidden;">JUMLAH BENAR : '.$jumlah_benar.'</div></td></tr>';
                        foreach ($qsoal->result() as $soal){
                            $pecahdijawab = '';
                            if (isset($pecahjawab[$no])) {
                                //$pecahlagijawaban = explode(';',$pecahjawab[$no]);
                                $pecahdijawab = (substr($pecahjawab[$no], -2) != 'NO') ? substr($pecahjawab[$no], -1) : '';
                             }
                            
                            echo '<tr>';
                            echo '<td style="text-align:center;"><div style="height: 17px; overflow:hidden;">'.$soal->soal_ke.'</div></td>';
                            echo '<td style="text-align:center;"><div style="height: 17px; overflow:hidden;">'.$soal->huruf_1.'</div></td>';
                            echo '<td style="text-align:center;"><div style="height: 17px; overflow:hidden;">'.$soal->huruf_2.'</div></td>';
                            echo '<td style="text-align:center;"><div style="height: 17px; overflow:hidden;">'.$soal->huruf_3.'</div></td>';
                            echo '<td style="text-align:center;"><div style="height: 17px; overflow:hidden;">'.$soal->huruf_4.'</div></td>';
                            echo '<td style="text-align:center;"><div style="height: 17px; overflow:hidden;">'.$soal->jawaban.'</div></td>';
                            echo '<td style="text-align:center;"><div style="height: 17px; overflow:hidden;">'.$pecahdijawab.'</div></td>';
                            echo '</tr>';
                            $no++;
                        }
                        echo '</table>';
                    } 
                    echo '</td>';
                }
                
                echo '</tr>';
                // SOAL

                echo '</table>';
                $kolomstart = $kolomstart + 3;
            

            }
        ?>

 
        
   
</body>
</html>