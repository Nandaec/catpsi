<div class="page-content">
    <div class="page-header">
        <h1><?=$judul;?></h1>
        
    </div><!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12">
        
        <div class="widget-box" id="widget-box-1">
            <div class="widget-header">
                <h5 class="widget-title"><?=$judul;?></h5>

                <div class="widget-toolbar no-border">
                    
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main">
                <div id="loading" class="text-center"></div>
                <table id="tbl_data" class="table table-striped table-bordered table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th>Peserta</th>
                            <?php for ($i=1;$i<=$jumlah_kolom;$i++) { ?>
                                <th class="text-center">Kolom <?=$i;?></th>
                            <?php } ?>
                            <th class="text-center">Hasil</th>
                            <th class="text-center">Download</th>
                        </tr>
                    </thead>    
                    <tbody>
                    <?php 
                    $no = 1;
                    $peserta = $this->db->query("SELECT a.peserta_id,b.first_name FROM peserta_ujian a JOIN users b ON a.peserta_id = b.id WHERE a.ujian_id = '$idujian' AND status = 'Y'");
                          foreach ($peserta->result() as $ps) { ?>
                    <tr>
                    <td class="text-center"><?=$no;?></td>
                    <td><?=$ps->first_name;?></td>
                    <?php for ($i=1;$i<=$jumlah_kolom;$i++) {
                        $jumlahbenar = $this->model_kueri->return_kolom_array('jumlah_benar','peserta_hasil',array('ujian_id'=>$idujian,'peserta_id'=>$ps->peserta_id,'kolom_soal'=>$i));?>
                        <td class="text-center"><?=$jumlahbenar;?></td>
                    <?php } ?>
                    <?php if($jenis == 'ujian'){ ?>
                    <td class="text-center"><?=$this->model_kueri->hasil_tes($ps->peserta_id,$idujian);?></td>
                    <?php }else{ ?>
                    <td class="text-center"><?=$this->db->query("SELECT sum(jumlah_benar) as jumlah_benar FROM peserta_hasil WHERE ujian_id='$idujian' AND peserta_id='$ps->peserta_id'")->row('jumlah_benar');?></td>
                    <?php } ?>
                    <td class="text-center"><a href="<?=site_url('expor/hasil/'.$idujian.'/'.$ps->peserta_id);?>">Download</a></td>
                    </tr>
                    <?php $no++; } ?>
                    </tbody>    
                    </table>
                </div>
            </div>
        </div>
            
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.page-content -->
<script type="text/javascript">
   var table;
   var save_method; 
   $(document).ready(function() {
    $('#tbl_data').DataTable();

   });
   </script>
