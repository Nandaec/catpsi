<div class="page-content">
    <div class="page-header">
        <h1><?= $judul; ?></h1>

    </div><!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12">

            <div class="widget-box" id="widget-box-1">
                <div class="widget-header">
                    <h5 class="widget-title">Skor </h5>
                    <div class="widget-toolbar no-border">
                    </div>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        <div id="container"></div>
                    </div>
                </div>
            </div>

        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.page-content -->
<script>
    Highcharts.chart('container', {
        title: {
            text: 'Grafik hasil tes',
            align: 'left'
        },
        subtitle: {
            text: '',
            align: 'left'
        },
        yAxis: {
            title: {
                text: 'Nilai'
            }
        },
        xAxis: {
            tickInterval: 1,
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                pointStart: 1,
                dataLabels: {
                    enabled: true,
                }
            }
        },
        series: [{
            name: 'Jawaban Benar',
            data: [
                <?php for ($i = 1; $i <= $jumlah_kolom; $i++) {
                    $jumlah_benar = $this->model_kueri->return_kolom_array('jumlah_benar', 'peserta_hasil', array('peserta_id' => $idpeserta, 'ujian_id' => $idujian, 'kolom_soal' => $i));
                ?>['Kolom <?= $i; ?>', <?= $jumlah_benar; ?>],
                <?php } ?>
            ]
        }],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    });
</script>