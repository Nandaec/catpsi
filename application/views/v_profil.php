<div class="page-content">
    <div class="page-header">
        <h1><?=$judul;?></h1>
        
    </div><!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12">
        
        <div class="widget-box" id="widget-box-1">
            <div class="widget-header">
                <h5 class="widget-title"><?=$judul;?></h5>
                <div class="widget-toolbar no-border">
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main">
                <table class="table table-bordered">
            <form action="#" id="form" role="form">
            <tbody>
                    <tr>
                        
                        <td width="30%">
                            Nama
                        </td>
                        <td>
                           <input class="form-control" type="text" id="nama" name="nama" value="<?=$this->ion_auth->user()->row()->first_name;?>">
                        </td>
                    </tr>
                    <tr>
                        
                        <td width="30%">
                            Username
                        </td>
                        <td>
                        <input class="form-control" type="text" id="username" name="username" value="<?=$this->ion_auth->user()->row()->username;?>">
                        </td>
                    </tr>
                    
                    <tr>
                        <td width="30%">
                            Password
                        </td>
                        <td>
                        <input class="form-control" id="password"  type="text" name="password" value="" placeholder="Kosongkan jika tidak ingin di update">
                        </td>
                    </tr>
                    
                    </form>
                    <tr>
                        <td width="30%">
                           
                        </td>
                        <td>
                        <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Update</button>
                        </td>
                    </tr>
                </tbody>
            
            </table>
                </div>
            </div>
        </div>
            
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.page-content -->

<script>
function save()
{
    
    $('#btnSave').text('saving...'); 
    $('#btnSave').attr('disabled',true); 
    $.ajax({
        url : '<?=site_url('profil/update');?>',
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(hasil)
        {
            console.log(hasil);
            if(hasil.status == 'berhasil') 
            {
                sukses('Data berhasil diperbaharui');
            }

            $('#btnSave').text('Simpan'); 
            $('#btnSave').attr('disabled',false); 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            salah('Eror Add/Update data');
            $('#btnSave').text('Simpan'); 
            $('#btnSave').attr('disabled',false); 

        }
    });
}
</script>