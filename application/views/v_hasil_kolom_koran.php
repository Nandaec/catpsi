<br>
<hr>
<?php if ($kolom->num_rows() > 0) { ?>
	<div class="row">
		<div class="col-xs-12 text-center">
			<div class="widget-box">
				<div class="widget-header">
					<h4 class="widget-title">Kolom</h4>
				</div>
			</div>
		</div>
	</div>
	<br>
	<?php $hr = $this->db->query("SELECT * FROM koran_ujian WHERE ujian_id = '$idujian' ORDER BY kolom ASC")->result();?>
	<div class="table-responsive scroll" style="height: 500px;">
		<table class="table table-sm table-bordered" style="width: 100vw;font-size: 14px;">
			<tbody>
				<tr>
					<td class="text-left" colspan="50"><strong>Nomor Urut</strong></td>
				</tr>
				<tr style="background-color: #d6d6d6;">
					<td class="text-left" style="color: #d6d6d6;">Jawaban</td>
					<td class="text-center">1</td>
					<td class="text-center">2</td>
					<td class="text-center">3</td>
					<td class="text-center">4</td>
					<td class="text-center">5</td>
					<td class="text-center">6</td>
					<td class="text-center">7</td>
					<td class="text-center">8</td>
					<td class="text-center">9</td>
					<td class="text-center">10</td>

					<td class="text-center">11</td>
					<td class="text-center">12</td>
					<td class="text-center">13</td>
					<td class="text-center">14</td>
					<td class="text-center">15</td>
					<td class="text-center">16</td>
					<td class="text-center">17</td>
					<td class="text-center">18</td>
					<td class="text-center">19</td>
					<td class="text-center">20</td>
				</tr>
			</tbody>
		</table>

		<table class="table table-sm table-bordered" style="width: 100vw;font-size: 14px;">
			<tbody>
			<?php foreach ($hr as $r) { 
				$jwb = $this->db->query("SELECT * FROM koran_jawaban WHERE ujian_id='$idujian' AND kolom='$r->kolom'")->row();?>
				<tr>
					<td class="text-center" colspan="1"><a class="btn btn-xs btn-danger" href="javascript:void(0)" title="HAPUS" onclick="hapus_kolom(<?=$r->id;?>)"><i class="fa fa-times" style="height: 5px !important;"></i></a></td>
					<td class="text-left" colspan="47"><strong>Kolom <?=$r->kolom;?></strong></td>
				</tr>
				<tr>
					<td class="text-left">Soal</td>
					<td class="text-center"><?=$r->s1;?></td>
					<td class="text-center"><?=$r->s2;?></td>
					<td class="text-center"><?=$r->s3;?></td>
					<td class="text-center"><?=$r->s4;?></td>
					<td class="text-center"><?=$r->s5;?></td>
					<td class="text-center"><?=$r->s6;?></td>
					<td class="text-center"><?=$r->s7;?></td>
					<td class="text-center"><?=$r->s8;?></td>
					<td class="text-center"><?=$r->s9;?></td>
					<td class="text-center"><?=$r->s10;?></td>

					<td class="text-center"><?=$r->s11;?></td>
					<td class="text-center"><?=$r->s12;?></td>
					<td class="text-center"><?=$r->s13;?></td>
					<td class="text-center"><?=$r->s14;?></td>
					<td class="text-center"><?=$r->s15;?></td>
					<td class="text-center"><?=$r->s16;?></td>
					<td class="text-center"><?=$r->s17;?></td>
					<td class="text-center"><?=$r->s18;?></td>
					<td class="text-center"><?=$r->s19;?></td>
					<td class="text-center"><?=$r->s20;?></td>
				</tr>

				<tr>
					<td class="text-left">Jawaban</td>
					<td class="text-center"><?=$jwb->s1;?></td>
					<td class="text-center"><?=$jwb->s2;?></td>
					<td class="text-center"><?=$jwb->s3;?></td>
					<td class="text-center"><?=$jwb->s4;?></td>
					<td class="text-center"><?=$jwb->s5;?></td>
					<td class="text-center"><?=$jwb->s6;?></td>
					<td class="text-center"><?=$jwb->s7;?></td>
					<td class="text-center"><?=$jwb->s8;?></td>
					<td class="text-center"><?=$jwb->s9;?></td>
					<td class="text-center"><?=$jwb->s10;?></td>

					<td class="text-center"><?=$jwb->s11;?></td>
					<td class="text-center"><?=$jwb->s12;?></td>
					<td class="text-center"><?=$jwb->s13;?></td>
					<td class="text-center"><?=$jwb->s14;?></td>
					<td class="text-center"><?=$jwb->s15;?></td>
					<td class="text-center"><?=$jwb->s16;?></td>
					<td class="text-center"><?=$jwb->s17;?></td>
					<td class="text-center"><?=$jwb->s18;?></td>
					<td class="text-center"><?=$jwb->s19;?></td>
					<td class="text-center"><?=$jwb->s20;?></td>
				</tr>
			<?php }?>
			</tbody>
		</table>
	</div>
<?php } else {

}
?>