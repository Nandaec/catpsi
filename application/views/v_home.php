<div class="page-content">
    <div class="page-header">
        <h1>Pesan dan Video Home Ujian</h1>
        
    </div><!-- /.page-header -->
    <div class="row">
        <div class="col-xs-8">
        <div class="wysiwyg-editor" id="editor1"><?=$home->pesan;?></div>
        <br>
        <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Update Pesan</button>
        </div><!-- /.col -->
        <div class="col-xs-4">
        <?php echo form_open_multipart('admin/update_video');?>
        <div class="form-group">
            <div class="col-xs-12">
            <label for="form-field-select-1">Ganti Video</label>
            <input type="file" name="video_name" size="20" />

            </div>
        </div>
        <br><br>
        <div class="form-actions center">
            <button type="submit" class="btn btn-sm btn-success">
                Upload
                <i class="ace-icon fa fa-upload icon-on-right bigger-110"></i>
            </button>
        </div>
        </div>
        </form>
    </div><!-- /.row -->
</div><!-- /.page-content -->

<div class="page-content">
    <div class="page-header">
        <h1>Pesan dan Video Home Test Koran</h1>
        
    </div><!-- /.page-header -->
    <div class="row">
        <div class="col-xs-8">
        <div class="wysiwyg-editor" id="editor2"><?=$home2->pesan;?></div>
        <br>
        <button type="button" id="btnSave2" onclick="save2()" class="btn btn-success">Update Pesan</button>
        </div><!-- /.col -->
        <div class="col-xs-4">
        <?php echo form_open_multipart('admin/update_video2');?>
        <div class="form-group">
            <div class="col-xs-12">
            <label for="form-field-select-1">Ganti Video</label>
            <input type="file" name="video_name2" size="20" />

            </div>
        </div>
        <br><br>
        <div class="form-actions center">
            <button type="submit" class="btn btn-sm btn-success">
                Upload
                <i class="ace-icon fa fa-upload icon-on-right bigger-110"></i>
            </button>
        </div>
        </div>
        </form>
    </div><!-- /.row -->
</div><!-- /.page-content -->

<script>
function save()
{
    var pesan = $("#editor1").html();
    console.log(pesan);
    $('#btnSave').text('saving...'); 
    $('#btnSave').attr('disabled',true); 
    $.ajax({
        url : '<?=site_url('admin/update_pesan');?>',
        type: "POST",
        data: {pesan : pesan},
        dataType: "JSON",
        success: function(hasil)
        {
            console.log(hasil);
            if(hasil.status == 'berhasil') 
            {
                alert('data pesan berhasil diupdate');
            }

            $('#btnSave').text('Simpan'); 
            $('#btnSave').attr('disabled',false); 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            salah('Eror Add/Update data');
            $('#btnSave').text('Simpan'); 
            $('#btnSave').attr('disabled',false); 

        }
    });
}
function save2()
{
    var pesan = $("#editor2").html();
    console.log(pesan);
    $('#btnSave2').text('saving...'); 
    $('#btnSave2').attr('disabled',true); 
    $.ajax({
        url : '<?=site_url('admin/update_pesan2');?>',
        type: "POST",
        data: {pesan : pesan},
        dataType: "JSON",
        success: function(hasil)
        {
            console.log(hasil);
            if(hasil.status == 'berhasil') 
            {
                alert('data pesan berhasil diupdate');
            }

            $('#btnSave2').text('Simpan'); 
            $('#btnSave2').attr('disabled',false); 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            salah('Eror Add/Update data');
            $('#btnSave2').text('Simpan'); 
            $('#btnSave2').attr('disabled',false); 

        }
    });
}
</script>

<script type="text/javascript">
    jQuery(function($){
        $('#editor1').ace_wysiwyg({
            toolbar:
            [
            'font',
            null,
            'fontSize',
            null,
            {name:'bold', className:'btn-info'},
            {name:'italic', className:'btn-info'},
            {name:'strikethrough', className:'btn-info'},
            {name:'underline', className:'btn-info'},
            null,
            {name:'insertunorderedlist', className:'btn-success'},
            {name:'insertorderedlist', className:'btn-success'},
            {name:'outdent', className:'btn-purple'},
            {name:'indent', className:'btn-purple'},
            null,
            {name:'justifyleft', className:'btn-primary'},
            {name:'justifycenter', className:'btn-primary'},
            {name:'justifyright', className:'btn-primary'},
            {name:'justifyfull', className:'btn-inverse'},
            null,
            {name:'createLink', className:'btn-pink'},
            {name:'unlink', className:'btn-pink'},
            null,
            {name:'insertImage', className:'btn-success'},
            null,
            'foreColor',
            null,
            {name:'undo', className:'btn-grey'},
            {name:'redo', className:'btn-grey'}
            ]
        });
        $(window).on('resize.editor', function() {
            var offset = $('#editor1').parent().offset();
            var winHeight =  $(this).height();

            $('#editor1').css({'height':winHeight - offset.top - 300, 'max-height': 'none'});
        }).triggerHandler('resize.editor');


        $('#editor2').ace_wysiwyg({
            toolbar:
            [
            'font',
            null,
            'fontSize',
            null,
            {name:'bold', className:'btn-info'},
            {name:'italic', className:'btn-info'},
            {name:'strikethrough', className:'btn-info'},
            {name:'underline', className:'btn-info'},
            null,
            {name:'insertunorderedlist', className:'btn-success'},
            {name:'insertorderedlist', className:'btn-success'},
            {name:'outdent', className:'btn-purple'},
            {name:'indent', className:'btn-purple'},
            null,
            {name:'justifyleft', className:'btn-primary'},
            {name:'justifycenter', className:'btn-primary'},
            {name:'justifyright', className:'btn-primary'},
            {name:'justifyfull', className:'btn-inverse'},
            null,
            {name:'createLink', className:'btn-pink'},
            {name:'unlink', className:'btn-pink'},
            null,
            {name:'insertImage', className:'btn-success'},
            null,
            'foreColor',
            null,
            {name:'undo', className:'btn-grey'},
            {name:'redo', className:'btn-grey'}
            ]
        });
        $(window).on('resize.editor', function() {
            var offset = $('#editor2').parent().offset();
            var winHeight =  $(this).height();

            $('#editor2').css({'height':300, 'max-height': 'none'});
        }).triggerHandler('resize.editor');
    });
</script>	
