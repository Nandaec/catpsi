<div class="page-content">
    <div class="page-header">
        <h1><?=$judul;?></h1>
        
    </div><!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12">
        
        <div class="widget-box" id="widget-box-1">
            <div class="widget-header">
                <h5 class="widget-title"><?=$judul;?></h5>

                <div class="widget-toolbar no-border">
                <button class="btn btn-sm btn-primary pull-right" type="button" onclick="tambah()">Tambah Data</button>

                    
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main">
                <div id="loading" class="text-center"></div>
                <table id="tbl_data" class="table table-striped table-bordered table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th>Nama Ujian</th>
                            <th>Tanggal</th>
                            <th>Jumlah Kolom</th>
                            <th>Durasi Kolom</th>
                            <th>Tampilan</th>
                            <th class="text-center">Jumlah Peserta</th>
                            <th width="15%"></th>
                        </tr>
                    </thead>    
                    <tbody>
                    </tbody>    
                    </table>
                </div>
            </div>
        </div>
            
        </div><!-- /.col -->
    </div><!-- /.row -->
</div>

<div class="modal inmodal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Modal title</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" role="form">
                    <input type="hidden" value="" name="id"/> 
                    <div class="row">
                        <div class="col-md-6">   
                            <div class="form-group">
                                <label>Kelas</label>
                                <select class="form-control" name="kelas" id="kelas">
                                    <?php foreach ($kelas as $kl) { ?>
                                    <option value="<?=$kl->id_kelas;?>"><?=$kl->nama_kelas;?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">   
                            <div class="form-group">
                                <label>Nama Ujian</label>
                                <input type="text" class="form-control" name="nama" id="nama">
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-3">   
                            <div class="form-group">
                                <label>Tanggal</label>
                                <div class="input-group">
                                    <input class="form-control date-picker" id="tanggal" type="text" name="tanggal" data-date-format="yyyy-mm-dd" autocomplete="off" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar bigger-110"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">   
                            <div class="form-group">
                                <label>Jumlah Kolom</label>
                                <input type="number" class="form-control" name="jumlah" id="jumlah">
                            </div>
                        </div>
                        <div class="col-md-3">   
                            <div class="form-group">
                                <label>Durasi per Kolom (menit)</label>
                                <input type="number" class="form-control" name="durasi" id="durasi">
                            </div>
                        </div>
                        <div class="col-md-3">   
                            <div class="form-group">
                                <label>Detik</label>
                                <input type="number" class="form-control" maxlength="2" name="detik" id="detik">
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-md-6">   
                            <div class="form-group">
                                <label>Tampilan Ujian</label>
                                <select class="form-control" name="keterangan" id="keterangan">
                                    <option value="Tampil semua">Tampil semua</option>
                                    <option value="Tampil per soal">Tampil per soal</option>    
                                </select>    
                            </div>
                        </div>
                    </div> -->    
                </form>        
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal  fade" id="modal_hapus">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title">Hapus Data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body">
                <p>Konfirmasi Penghapusan Data</p>
                <form action="#" id="formdelete">
                    <input type="hidden" name="iddelete" value="">  
                </form> 
            </div>
            <div class="modal-footer">
                <button type="button" id="btnDelete" class="btn btn-danger float-right" onclick="hapus()">Hapus</button>
                <button type="button" class="btn btn-info" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div> 

<script type="text/javascript">
    var table;
    var save_method; 
    $(document).ready(function() {
        table = $('#tbl_data').DataTable({ 
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('admin/ujian_list_koran')?>",
                "type": "POST"
            },
            "columnDefs": [
            { 
                "targets": [ 0 ],
                "orderable": false,
            },
            ]
        });
    });

    function reload_table()
    {
        table.ajax.reload(null,false); 

    } 

    function tambah()
    {
        save_method = 'add';
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error'); 
        $('.help-block').empty();
        $('#detik').val('00');
        $('#modal_form').modal('show'); 
        $('.modal-title').text('Tambah Ujian'); 
    }

    function save()
    {
        validasi();

        if(save_method == 'add') {

            url = "<?php echo site_url('admin/ujian_add_koran')?>";
        } else {

            url = "<?php echo site_url('admin/ujian_update')?>";
        }   
        $('#btnSave').text('saving...'); 
        $('#btnSave').attr('disabled',true); 
        
        $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(hasil)
            {
                console.log(hasil);
                if(hasil.status == 'berhasil') 
                {
                    $('#modal_form').modal('hide');
                    reload_table();

                }
                $('#modal_form').modal('hide');
                $('#btnSave').text('Simpan'); 
                $('#btnSave').attr('disabled',false); 


            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                salah('Eror Add/Update data');
                $('#btnSave').text('Simpan'); 
                $('#btnSave').attr('disabled',false); 

            }
        });
    }

    function edit_data(id)
    {
        save_method = 'update';
        $('#form')[0].reset(); 
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();

        $.ajax({
            url : "<?php echo site_url('admin/ujian_edit')?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {

                $('[name="id"]').val(data.id_ujian);
                $('[name="kelas"]').val(data.kelas_id).trigger('change');
                $('[name="nama"]').val(data.nama_ujian);
                $('#tanggal').datepicker().datepicker('setDate', data.tanggal);
                $('[name="jumlah"]').val(data.jumlah_kolom);
                $('[name="durasi"]').val(data.durasi_kolom);
                $('[name="detik"]').val(data.durasi_detik);
                $('[name="keterangan"]').val(data.keterangan).trigger('change');
                $('#modal_form').modal('show');
                $('.modal-title').text('Edit Ujian'); 

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                salah('Gagal mengambil data');
            }    
        });
    }

    function konfirm_hapus(id)
    {
        $('[name="iddelete"]').val(id);
        $('.modal-title').text('Hapus Data');
        $('#modal_hapus').modal('show');

    }

    function hapus()
    {
        var url = "<?php echo site_url('admin/ujian_delete')?>";
        $('#btnDelete').text('Menghapus...'); 
        $('#btnDelete').attr('disabled',true); 

        $.ajax({
            url : url,
            type: "POST",
            data: $('#formdelete').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                console.log(data);
                if(data.status == 'berhasil') 
                {
                    $('#modal_hapus').modal('hide');
                    reload_table();

                }
                $('#btnDelete').text('Hapus'); 
                $('#btnDelete').attr('disabled',false); 
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                salah('Data telah digunakan sistem');
                $('#btnDelete').text('Hapus'); 
                $('#btnDelete').attr('disabled',false); 

            }
        });
    } 

    function validasi()
    {
        if ( $('#nama').val() == '') { alert('Nama Ujian harus diisi'); return FALSE; }
    }
</script>