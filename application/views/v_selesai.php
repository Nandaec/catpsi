<div class="page-content">
    <div class="page-header">
        <h1><?=$judul;?></h1>
        
    </div><!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12">
        
        <div class="widget-box" id="widget-box-1">
            <div class="widget-header">
                <h5 class="widget-title">Skor </h5>
                <div class="widget-toolbar no-border">
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main">
                <div id="container"></div>
                </div>
            </div>
        </div>
            
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.page-content -->
<script>
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Skor'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: 0,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    credits: {
     enabled: false
     },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Jawaban benar : <b>{point.y}</b>'
    },
    series: [{
        name: 'Kolom',
        data: [
            <?php for($i=1;$i<=$jumlah_kolom;$i++) { 
              $jumlah_benar = $this->model_kueri->return_kolom_array('jumlah_benar','peserta_hasil',array('peserta_id'=>$idpeserta,'ujian_id'=>$idujian,'kolom_soal'=>$i));    
            ?>
            ['Kolom <?=$i;?>', <?=$jumlah_benar;?>],
           <?php } ?>
        ],
        dataLabels: {
            enabled: true,
            rotation: 1,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '22px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});
</script>