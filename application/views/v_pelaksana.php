<div class="row">
    <div class="col-12">
        
        <div class="card">
            <div class="card-body">
                
                    <table id="tbl_data" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th class="text-center">No</th>
                        <th>Nama Pelaksana</th>
                        <th class="text-center" width="15%">Jumlah Sertifikat</th>
                    </tr>
                    </thead>    
                    <tbody>
                    <?php if ($rows) { 
                        $no = 1;
                        foreach ($rows as $row){ ?>
                    <tr>
                    <td class="text-center" width="10%"><?=$no;?></td>
                    <td><?=$row->nama_pelaksana;?></td>
                    <td class="text-center"><?=$this->model_kueri->cek_jumlah_array_minimal('id_sertifikat','sertifikat',array('pelaksana'=>$row->nama_pelaksana));?></td>
                    </tr>
                    <?php $no++; } } ?>
                    </tbody>    
                    </table>
                

            </div>
        </div>
    </div>
</div>