<div class="page-content">
	<div class="page-header">
		<h1><?=$judul;?></h1>		
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="widget-box" id="widget-box-1">
				<div class="widget-header">
					<h5 class="widget-title"><?=$judul;?></h5>
				</div>
				<div class="widget-body">
					<div class="row">
						<div class="col-xs-12">
							<br>
							<form class="form-horizontal" role="form" id="formkolom">
								<input type="hidden" name="idujian" value="<?=$idujian;?>">
								<div class="form-group">
									<label class="col-sm-3 control-label no-padding-right"> Kolom </label>
									<div class="col-sm-8">
										<select name="kolom" id="kolom" class="form-control">
											<option value=''></option>
											<?php for ($i=1;$i<=$jumkolom;$i++) {
												echo "<option value='".$i."'>Kolom ".$i."</option>";
											} ?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label no-padding-right"> Baris </label>
									<div class="col-sm-8">
										<input type="text" placeholder="baris" name="baris" id="baris" class="col-xs-10 col-sm-5" value="20" onkeyup="getSoal()" readonly="" />
									</div>
								</div>

								<br>

								<div class="d-none" id="tamp_soal">
									<table class="table table-sm">
										<tbody>
											<tr>
												<td colspan="20">
													<input type="text" class="form-control form-control-sm border-none" name="inp_kolom" id="inp_kolom">
												</td>
											</tr>
											<tr>
												<td>
													<input type="text" class="form-control border-none" value="Nomor">
												</td>
												<td id="tabel_nomor"></td>
											</tr>
											<tr>
												<td>
													<input type="text" class="form-control border-none" value="Soal">
												</td>
												<td id="tabel_soal"></td>
											</tr>
											<tr class="d-none">
												<td>
													<input type="text" class="form-control border-none" value="Jawaban">
												</td>
												<td id="tabel_jawab"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</form>
							<div class="clearfix form-actions">
								<div class="col-md-offset-3 col-md-9">
									<button class="btn btn-info" type="button" id="btnSave" onclick="savekolom()">
										Tambahkan
									</button>
								</div>
							</div>
						</div>
						<div id="loading" class="text-center"></div>
						<div class="col-xs-12">
							<div id="hasilkolom"></div>
						</div>
					</div>
				</div>
			</div>            
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		hasil_kolom_koran();
		$('#kolom').change(function (e) {
			e.preventDefault()
			getSoal()
			$(`#inp_soal${0}`).focus()
		});
	})
	function isNumber(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
		return true;
	}
	function getSoal(){
		var html_soal = '';
		var html_jawab = '';
		var html_nomor = '';
		let kolom = $(`#kolom`).val()
		let baris = $(`#baris`).val()
		if(kolom){
			$('#tamp_soal').removeClass('d-none');

			$('#inp_kolom').val(`Soal kolom ${kolom}`)

			for (i = 0; i < baris; i++) {
				html_nomor += `
					<td>
						<input type="text" class="form-control text-center bg-white border-none" value="${i+1}" readonly>
					</td>
				`
			}
			$("#tabel_nomor").html(html_nomor)

			for (i = 0; i < baris; i++) {
				html_soal += `
					<td>
						<input type="text" class="form-control text-center" name="inp_soal[]" id="inp_soal${i}" onkeypress="return isNumber(event)" onkeyup="set_jawaban(${i})" autocomplete="off">
					</td>
				`
			}
			$("#tabel_soal").html(html_soal)

			for (i = 0; i < baris; i++) {
				if(i==0){
					html_jawab += `
						<td>
							<input type="text" class="form-control border-none bg-white" name="inp_jawab[]" id="inp_jawab${i}"  readonly>
						</td>
					`
				}else{
					html_jawab += `
						<td>
							<input type="text" class="form-control" name="inp_jawab[]" id="inp_jawab${i}" readonly>
						</td>
					`
				}
			}
			$("#tabel_jawab").html(html_jawab)
		}else{
			$('#tamp_soal').addClass('d-none');
		}
	}
	function Keyboard(i){
		$(`#inp_soal${i}`).keyup((e)=>{
			keyCode = e.keyCode || e.which;
			if (keyCode == 9 || e.code=="Tab") { 
				e.preventDefault(); 
				$(`#inp_soal${i+1}`).focus()
			} 
		})
	}
	function set_jawaban(i){
		$(`#inp_soal${i+1}`).focus()
		let soal = $(`#inp_soal${i}`).val() ? $(`#inp_soal${i}`).val() : 0;
		let soal_min = $(`#inp_soal${i-1}`).val() ? $(`#inp_soal${i-1}`).val() : 0;

		if(i>0){
			if(soal && soal_min){
				$(`#inp_jawab${i}`).val(parseInt(soal_min)+parseInt(soal))
			}else{
				$(`#inp_jawab${i}`).val('')
			}
		}
	}

	function savekolom()
	{
		validasikolom();  
		url = "<?php echo site_url('admin/kolom_koran_add')?>";

		$('#btnSave').text('saving...'); 
		$('#btnSave').attr('disabled',true); 

		$.ajax({
			url : url,
			type: "POST",
			data: $('#formkolom').serialize(),
			dataType: "JSON",
			success: function(hasil)
			{
				console.log(hasil);
				if(hasil.status == 'berhasil') 
					{  hasil_kolom_koran();
						alert('Berhasil menyimpan soal')
					} else {
						alert('gagal menyimpan data kolom')
					}

					$('#btnSave').text('Tambahkan'); 
					$('#btnSave').attr('disabled',false); 
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					alert('Eror Add/Update data');
					$('#btnSave').text('Tambahkan'); 
					$('#btnSave').attr('disabled',false); 
				}
			});
	}
	function validasikolom()
	{  
		if ( $('#kolom').val() == '') { alert('Kolom harus diisi'); return FALSE; }  
	}

	function hasil_kolom_koran()
	{
		var idujian = <?=$idujian;?>;
		var urlhasil =  "<?php echo site_url('admin/hasil_kolom_koran')?>";
		$("#hasilkolom").load(urlhasil,{idujian : idujian},function(){
		});

	}
	function hapus_kolom(id)
	{
		$.ajax({
			url : "<?php echo site_url('admin/kolom_koran_delete')?>",
			type: "POST",
			data: "idkolom="+id,
			dataType: "JSON",
			success: function(data)
			{
				if(data.status == 'berhasil') 
				{
					hasil_kolom_koran();
				}
			}
		});
	} 
</script>