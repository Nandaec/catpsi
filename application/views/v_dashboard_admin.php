<div class="page-content">
    <div class="page-header">
        <h1><?=$judul;?></h1>
        
    </div><!-- /.page-header -->
    <div class="row">
        <div class="col-xs-6">
        
        <div class="widget-box" id="widget-box-1">
            <div class="widget-header">
                <h5 class="widget-title">Data</h5>

                <div class="widget-toolbar no-border">
                </div>
            </div>

            <div class="widget-body">
            <div class="widget-main no-padding">
            <table class="table table-striped table-bordered table-hover">
                <thead class="thin-border-bottom">
                    <tr>
                        <th>
                            <i class="ace-icon fa fa-user"></i>
                            Jumlah User
                        </th>

                        <th>
                            <?=$jumpeserta;?>
                        </th>
                    </tr>
                    <tr>
                        <th>
                            <i class="ace-icon fa fa-user"></i>
                            Jumlah Kelas
                        </th>

                        <th>
                            <?=$this->model_kueri->cek_jumlah_array('kelas',array());?>
                        </th>
                    </tr>
                    <tr>
                        <th>
                            <i class="ace-icon fa fa-user"></i>
                            Jumlah Ujian
                        </th>

                        <th>
                            <?=$this->model_kueri->cek_jumlah_array('ujian',array());?>
                        </th>
                    </tr>
                </thead>

                
            </table>
        </div>
            </div>
        </div>
            
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.page-content -->