<div class="page-content">
    <div class="page-header">
        <h1><?=$judul;?></h1>
        
    </div><!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12">
        
        <div class="widget-box" id="widget-box-1">
            <div class="widget-header">
                <h5 class="widget-title"><?=$judul;?></h5>

                <div class="widget-toolbar no-border">
                <button class="btn btn-sm btn-primary pull-right" type="button" onclick="tambah()">Tambah Data</button>

                    
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main">
                <table id="tbl_data" class="table table-striped table-bordered table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>Aktif</th>
                            <th width="10%"></th>
                        </tr>
                    </thead>    
                    <tbody>
                    </tbody>    
                    </table>
                </div>
            </div>
        </div>
            
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.page-content -->
<script type="text/javascript">
   var table;
   var save_method; 
   $(document).ready(function() {
            table = $('#tbl_data').DataTable({ 
               
              "processing": true, //Feature control the processing indicator.
              "serverSide": true, //Feature control DataTables' server-side processing mode.
              "order": [], //Initial no order.
          
              // Load data for the table's content from an Ajax source
              "ajax": {
                "url": "<?php echo site_url('admin/user_list')?>",
                "type": "POST"
              },
          
              //Set column definition initialisation properties.
              "columnDefs": [
              { 
                "targets": [ 0 ], //last column
                "orderable": false, //set not orderable
              },
              ]
          
            });

   });

function reload_table()
{
    table.ajax.reload(null,false); 
    
}            
  function tambah()
{
    save_method = 'add';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error'); 
    $('.help-block').empty();
    $('#modal_form').modal('show'); 
    $('.modal-title').text('Tambah User'); 
}

function save()
{
    validasi();
  
    if(save_method == 'add') {
    
        url = "<?php echo site_url('admin/user_add')?>";
    } else {
        
    url = "<?php echo site_url('admin/user_update')?>";
    }   
    $('#btnSave').text('saving...'); 
    $('#btnSave').attr('disabled',true); 
        
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(hasil)
        {
            console.log(hasil);
            if(hasil.status == 'berhasil') 
            {
                $('#modal_form').modal('hide');
                reload_table();

            }

            $('#btnSave').text('Simpan'); 
            $('#btnSave').attr('disabled',false); 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            salah('Eror Add/Update data');
            $('#btnSave').text('Simpan'); 
            $('#btnSave').attr('disabled',false); 

        }
    });
}

function edit_user(id)
{
    save_method = 'update';
    $('#form')[0].reset(); 
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('admin/user_edit')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            
            $('[name="id"]').val(data.id);
            $('[name="username"]').val(data.username);
            $('[name="nama"]').val(data.first_name);
            $('[name="status"]').val(data.active).trigger('change');
            $('#modal_form').modal('show');
            $('.modal-title').text('Edit User'); 

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            salah('Gagal mengambil data');
        }    
    });
}

function delete_user(id)
{
  $('[name="iddelete"]').val(id);
  $('.modal-title').text('Hapus Data');
  $('#modal_hapus').modal('show');

}

function hapus()
{
    var url = "<?php echo site_url('admin/user_delete')?>";
    $('#btnDelete').text('Menghapus...'); 
    $('#btnDelete').attr('disabled',true); 
    
    $.ajax({
        url : url,
        type: "POST",
        data: $('#formdelete').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            console.log(data);
            if(data.status == 'berhasil') 
            {
                $('#modal_hapus').modal('hide');
                reload_table();

            }

            $('#btnDelete').text('Hapus'); 
            $('#btnDelete').attr('disabled',false); 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            salah('Data telah digunakan sistem');
            $('#btnDelete').text('Hapus'); 
            $('#btnDelete').attr('disabled',false); 

        }
    });
} 

function validasi()
{
  
  if ( $('#username').val() == '') { salah('Username harus diisi'); return FALSE; }
  if ( $('#nama').val() == '') { salah('Nama harus diisi'); return FALSE; }
  
}
     
 </script>
 <div class="modal inmodal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
           <h4 class="modal-title">Modal title</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
            <div class="modal-body form">
                <form action="#" id="form" role="form">
                <input type="hidden" value="" name="id"/> 
                 <div class="row">
                    <div class="col-md-6">   
                      <div class="form-group">
                      <label>Nama</label>
                      <input type="text" class="form-control" name="nama" id="nama">
                      </div>
                  </div>
                  <div class="col-md-6">   
                      <div class="form-group">
                      <label>Username</label>
                      <input type="text" class="form-control" name="username" id="username">
                      </div>
                  </div>
                  
                 </div>
                 <div class="row">
                    <div class="col-md-6">   
                        <div class="form-group">
                        <label>Password</label>
                        <input type="text" class="form-control" name="password" id="password">
                        </div>
                    </div>
                    <div class="col-md-6">   
                      <div class="form-group">
                      <label>Status</label>
                        <select name="status" class="form-control">
                            <option value="1">Aktif</option>
                            <option value="0">Tidak Aktif</option>
                        </select>
                      </div>
                    </div>
                    
                 </div>
                   
                </form>        
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal  fade" id="modal_hapus">
          <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-danger">
            <h4 class="modal-title">Hapus Data</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
            <p>Konfirmasi Penghapusan Data</p>
            <form action="#" id="formdelete">
            <input type="hidden" name="iddelete" value="">  
            </form> 
            </div>
            <div class="modal-footer">
            <button type="button" id="btnDelete" class="btn btn-danger float-right" onclick="hapus()">Hapus</button>
            <button type="button" class="btn btn-info" data-dismiss="modal">Tutup</button>
            </div>
          </div>
          <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
</div> 