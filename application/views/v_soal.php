<div class="row" id="tabelkolom" style="background-color:#FFF;">
    <div class="col-xs-6">
        <table id="simple-table" class="table table-bordered table-hover">
        <tr>
        <td class="text-center" colspan="5" style="line-height: 30px;font-size:16px;"><b>Kolom <?=$kolom;?></b></td>
        </tr>
        <tr>
        <?php foreach ($ujikolom as $uk) { ?>
        <td class="text-center" style="line-height: 30px;font-size:16px;"><strong><?=$uk->kolom_nilai;?></strong></td>
        <?php } ?>
        </tr>
        <tr>
        <?php foreach ($ujikolom as $uk) { ?>
        <td class="text-center" style="line-height: 30px;font-size:16px;"><strong><?=$uk->huruf;?></strong></td>
        <?php } ?>
        </tr>
        </table>
    </div>
    <div class="col-xs-6">
        <table id="simple-table" class="table table-bordered table-hover">
        <tr>
        <td class="text-center" style="line-height: 30px;font-size:16px;"><b><?=$this->ion_auth->user()->row()->first_name;?></b></td>
        </tr>
        <tr>
        <td class="text-center" style="line-height: 30px;font-size:16px;">SISA WAKTU</td>
        </tr>
        <tr>
        <td class="text-center" style="line-height: 30px;font-size:16px;"><div id="countdown"></div></td>
        </tr>
        </table>
    </div>
</div>
<form id="formjawab" method="post">
        <input type="hidden" name="jumsoal" value="<?=$jumsoal;?>">
        <input type="hidden" name="idujian" value="<?=$idujian;?>">
        <input type="hidden" name="kolomujian" value="<?=$kolom;?>">
<?php foreach ($soals->result() as $sl) { ?>
    <input type="hidden" id="soalke<?=$sl->soal_ke;?>" name="soalke<?=$sl->soal_ke;?>" value="">
    <input type="hidden" id="jawabke<?=$sl->soal_ke;?>" name="jawabke<?=$sl->soal_ke;?>" value="">
<?php } ?>
</form>
<div class="row">
    <div class="col-xs-6">
        <table id="simple-table" class="table table-bordered table-hover table-striped" style="font-weight: bold;">
        <thead>
            <tr>
                <th class="center">
                NO   
                </th>
                <th class="center" colspan="4">
                SOAL  
                </th>
                <th class="center" width="15%">
                JAWABAN 
                </th>
            </tr>
        </thead>
        <tbody>
        
        <?php 
        $no = 1;
        foreach ($soals->result() as $sl) { ?>
        
        <tr>
        <td class="text-center" style="line-height: 30px; width:10%; font-size:16px;"><?=$sl->soal_ke;?></td>
        <td class="text-center" style="line-height: 30px; font-size:16px;"><?=$sl->huruf_1;?></td>
        <td class="text-center" style="line-height: 30px;font-size:16px;"><?=$sl->huruf_2;?></td>
        <td class="text-center" style="line-height: 30px;font-size:16px;"><?=$sl->huruf_3;?></td>
        <td class="text-center" style="line-height: 30px;font-size:16px;"><?=$sl->huruf_4;?></td>
        <td class="text-center" id="jawabpeserta<?=$no;?>" style="line-height: 30px;font-size:16px;"></td>
        </tr>
        <?php $no++; } ?>
        
        </tbody>
        </table>
        
    </div>

    <div class="col-xs-6">
        <table id="simple-table" class="table table-bordered table-hover table-striped" style="font-weight: bold;">
        <thead>
            <tr>
                <th class="center">
                NO   
                </th>
                <th class="center" colspan="5">
                PILIHAN
                </th>
            </tr>
        </thead>
        <tbody>
        <?php 
        for ($i=1;$i<=$jumsoal;$i++) { ?>
        <tr>
        <td class="text-center" width="12%" style="line-height: 30px;font-size:16px;"><?=$i;?></td>
        <td class="text-center"><button class="btn btn-xs btn-primary btnjawab" data-soal="<?=$i;?>" data-huruf="A">&nbsp; A &nbsp;</button></td>
        <td class="text-center"><button class="btn btn-xs btn-primary btnjawab" data-soal="<?=$i;?>" data-huruf="B">&nbsp; B &nbsp;</button></td>
        <td class="text-center"><button class="btn btn-xs btn-primary btnjawab" data-soal="<?=$i;?>" data-huruf="C">&nbsp; C &nbsp;</button></td>
        <td class="text-center"><button class="btn btn-xs btn-primary btnjawab" data-soal="<?=$i;?>" data-huruf="D">&nbsp; D &nbsp;</button></td>
        <td class="text-center"><button class="btn btn-xs btn-primary btnjawab" data-soal="<?=$i;?>" data-huruf="E">&nbsp; E &nbsp;</button></td>
        </tr>
        <?php $no++; } ?>
        </tbody>
        </table>
    </div>

</div>
<script>
$(document).ready(function() {
        var timer2 = localStorage.getItem('timer');
        if(timer2 === null) timer2 = "<?=$durasi;?>:<?=$detik;?>";
        $('#countdown').html(timer2);

        var interval = setInterval(function() {
            var timer = timer2.split(':');
            var minutes = parseInt(timer[0], 10);
            var seconds = parseInt(timer[1], 10);
            --seconds;
            minutes = (seconds < 0) ? --minutes : minutes;
            if (minutes < 0){
                clearInterval(interval);
                localStorage.removeItem('timer');
                next_kolom();
                $('html, body').animate({ scrollTop: 0 }, 'fast');
            }else{
                seconds = (seconds < 0) ? 59 : seconds;
                seconds = (seconds < 10) ? '0' + seconds : seconds;
                $('#countdown').html(minutes + ':' + seconds);
                timer2 = minutes + ':' + seconds;
                localStorage.setItem('timer',timer2);
            }
        }, 1000);
});

function simpan()
{
    $.ajax({
        url : '<?=site_url('peserta/simpan_jawaban');?>',
        type: "POST",
        data: $('#formjawab').serialize(),
        dataType: "JSON",
        success: function(data)
        {
           
        }
    });


}

$( ".btnjawab" ).click(function() {
  var jawaban = $(this).attr("data-huruf");
  var idjawaban = $(this).attr("data-soal");
  $('#jawabpeserta'+idjawaban).html(jawaban);
  $('#soalke'+idjawaban).val(idjawaban);
  $('#jawabke'+idjawaban).val(jawaban);
  simpan();
});

function next_kolom()
{
    var kolomke = $('#kolomke').val();
    kolomnext = parseInt(kolomke)+1;
    //localStorage.setItem("kolomke", kolomnext);
    $('#kolomke').val(kolomnext);
    get_soal();
}

window.onscroll = function() {myFunction()};

var navbar = document.getElementById("tabelkolom");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}


</script>        
