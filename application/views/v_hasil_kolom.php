<br>
<hr>
<?php if ($kolom->num_rows() > 0) { ?>
    <div class="row">
    <div class="col-xs-12 text-center">
        <div class="widget-box">
            <div class="widget-header">
                <h4 class="widget-title">Kolom</h4>
            </div>

            <div class="widget-body">
                <div class="widget-main">
                    <form class="form-inline">
                        Jumlah Soal : <input type="number" placeholder="Jumlah Soal" id="jumlahsoal" value="10" />
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
    <br>
<?php
      foreach ($kolom->result() as $kl) { 
      $ceksoal = $this->db->query("SELECT * FROM ujian_soal WHERE ujian_id = '$idujian' AND kolom = '$kl->kolom'");    
      ?>
      <div class="col-xs-4">
      <table id="simple-table" class="table  table-bordered table-hover">
      <tr>
      <td class="text-center"><strong>Kolom <?=$kl->kolom;?></strong></td>
      <td class="text-center">
          <button class="btn btn-xs btn-primary" id="btnGenerate" onclick="generate_soal(<?=$kl->kolom;?>)">
                Buat  Soal
            </button>
      </td>
      <td class="text-center"><?=($ceksoal->num_rows() > 0) ? '<a href="javascript:void(0)" onclick="lihat_soal('.$idujian.','.$kl->kolom.')">Lihat Soal</a>' : 'Belum ada soal';?></td>
      </tr>
      <?php $hr = $this->db->query("SELECT * FROM ujian_kolom WHERE ujian_id = '$idujian' AND kolom = '$kl->kolom' ORDER BY huruf ASC")->result();
            foreach ($hr as $r) { ?>
            <tr>
            <td class="text-center" style="font-size:15px;"><?=$r->huruf;?></td>
            <td class="text-center" style="font-size:15px;"><b><?=$r->kolom_nilai;?></b></td>
            <td class="text-center"><a class="btn btn-xs btn-danger" href="javascript:void(0)" title="HAPUS" onclick="hapus_kolom(<?=$r->id_ujian_kolom;?>)"><i class="fa fa-times"></i></a></td>
            </tr>
           <?php }
      ?>
      
      </table>
      </div>
      <?php    
      } 
} else {

}
?>