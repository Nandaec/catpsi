<?php
//date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');
class Mylibrary{
function base_uerel($atRoot=FALSE, $atCore=FALSE, $parse=FALSE){
	if (isset($_SERVER['HTTP_HOST'])) {
		$http = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
		$hostname = $_SERVER['HTTP_HOST'];
		$dir =  str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);
		$core = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), NULL, PREG_SPLIT_NO_EMPTY);
		$core = $core[0];
		$tmplt = $atRoot ? ($atCore ? "%s://%s/%s/" : "%s://%s/") : ($atCore ? "%s://%s/%s/" : "%s://%s%s");
		$end = $atRoot ? ($atCore ? $core : $hostname) : ($atCore ? $core : $dir);
		$base_url = sprintf( $tmplt, $http, $hostname, $end );
	}
	else $base_url = 'http://localhost/';
	if ($parse) {
		$base_url = parse_url($base_url);
		if (isset($base_url['path'])) if ($base_url['path'] == '/') $base_url['path'] = '';
	}
	return $base_url;
}
	
	
	
 function persen($atas,$bawah)
{
	$persen = 0.00;
	if ($atas != 0 && $bawah != 0)
	{
		$persen = round($atas/$bawah * 100,2);		
	}
	return $persen.' %';
} 

 function persensaja($atas,$bawah)
{
	$persen = 0.00;
	if ($atas != 0 && $bawah != 0)
	{
		$persen = round($atas/$bawah * 100,2);		
	}
	return $persen;
} 
 
 function tanggallahir($tanggal)
{
	$pecahkan = explode('|', $tanggal);
	$tgl = $pecahkan[0];
	$bln = $pecahkan[1];
	$tahun = $pecahkan[2];
	return $tahun.'-'.$bln.'-'.$tgl;
	
} 


	
	function format_rupiah($angka){
	  $rupiah=number_format($angka,0,',','.');
	  return $rupiah;
	}

	function format_dollar($angka){
		$rupiah=number_format($angka,0,',',',');
		return $rupiah;
	  }

	function getBulan($bln){
			switch ($bln){
					case 1: 
						return "Januari";
						break;
					case 2:
						return "Februari";
						break;
					case 3:
						return "Maret";
						break;
					case 4:
						return "April";
						break;
					case 5:
						return "Mei";
						break;
					case 6:
						return "Juni";
						break;
					case 7:
						return "Juli";
						break;
					case 8:
						return "Agustus";
						break;
					case 9:
						return "September";
						break;
					case 10:
						return "Oktober";
						break;
					case 11:
						return "November";
						break;
					case 12:
						return "Desember";
						break;
			}
	}

	function getBulanShort($bln){
		switch ($bln){
				case 1: 
					return "Jan";
					break;
				case 2:
					return "Feb";
					break;
				case 3:
					return "Mar";
					break;
				case 4:
					return "Apr";
					break;
				case 5:
					return "Mei";
					break;
				case 6:
					return "Jun";
					break;
				case 7:
					return "Jul";
					break;
				case 8:
					return "Agt";
					break;
				case 9:
					return "Sep";
					break;
				case 10:
					return "Okt";
					break;
				case 11:
					return "Nov";
					break;
				case 12:
					return "Des";
					break;
		}
    }

	function tgl_indo($tgl){
		$tanggal = substr($tgl,8,2);
		$bulan = $this->getBulan(substr($tgl,5,2));
		$tahun = substr($tgl,0,4);
		return $tanggal.' '.$bulan.' '.$tahun;		 
	}	

	function tgl_indoo($tgl){
		$tanggal = substr($tgl,8,2);
		$bulan = $this->getBulan(substr($tgl,5,2));
		$tahun = substr($tgl,0,4);
		return $tanggal.'/'.$bulan;		 
	}
	
	function waktu_timespan($timespan)
	{
		$tgl = date('d',$timespan);
		$bulan = $this->getBulan(date('m',$timespan));
		$tahun = date('Y',$timespan);
		$jam = date('H:i',$timespan);
		return $tgl.' '.$bulan.' '.$tahun.' '.$jam;
	}
	
	function hari_tanggal($tanggal)
	{
  
		$day = date('D', strtotime($tanggal));
		$dayList = array(
			'Sun' => 'Minggu',
			'Mon' => 'Senin',
			'Tue' => 'Selasa',
			'Wed' => 'Rabu',
			'Thu' => 'Kamis',
			'Fri' => 'Jumat',
			'Sat' => 'Sabtu'
		);
		return $dayList[$day];
		
	}
}





