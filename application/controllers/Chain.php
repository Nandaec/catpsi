<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chain extends CI_Controller {
    
	public function jabatan() {
		$this->db->select('*');
		$this->db->order_by('nama_jabatan', 'asc');
		$query = $this->db->get('jabatan');
		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				echo '<option value="'.$row->nama_jabatan.'">'.$row->nama_jabatan.'</option>';
			}
	     }	
	}

	public function unit() {
		$this->db->select('*');
		$this->db->order_by('nama_unit', 'asc');
		$query = $this->db->get('unit');
		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				echo '<option value="'.$row->nama_unit.'">'.$row->nama_unit.'</option>';
			}
	     }	
	}

	public function dahan() {
		$this->db->select('*');
		$this->db->order_by('nama_dahan', 'asc');
		$query = $this->db->get('dahan');
		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				echo '<option value="'.$row->nama_dahan.'">'.$row->nama_dahan.'</option>';
			}
	     }	
	}

	public function profesi() {
		$this->db->select('*');
		$this->db->order_by('nama_profesi', 'asc');
		$query = $this->db->get('profesi');
		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				echo '<option value="'.$row->nama_profesi.'">'.$row->nama_profesi.'</option>';
			}
	     }	
	}

	public function pelaksana() {
		$this->db->select('*');
		$this->db->order_by('nama_pelaksana', 'asc');
		$query = $this->db->get('pelaksana');
		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				echo '<option value="'.$row->nama_pelaksana.'">'.$row->nama_pelaksana.'</option>';
			}
	     }	
	}

	public function cari_pegawai() {
		$nip = trim($this->input->post('nip'));
		$hasil = 0;
		$id_pegawai = '';
		$nama = '';
		$unit = '';
		$jabatan = '';
		$this->db->select('*');
        $this->db->from('pegawai');
		$this->db->where('nip',$nip);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			$hasil = 1;
			foreach ($query->result() as $row)
			{
				$id_pegawai = $row->id_pegawai;
				$nama = $row->nama;
				$unit = $row->unit;
				$jabatan = $row->jabatan;
			}
		 }
		$data = array('hasil'=>$hasil,'id_pegawai'=>$id_pegawai,'nip'=>$nip,'nama'=>$nama,'unit'=>$unit,'jabatan'=>$jabatan);
		echo json_encode($data);  	
	}
			
			
	
}