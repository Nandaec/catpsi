<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {
    function __construct(){
        parent::__construct();
        if (!$this->ion_auth->logged_in()) { redirect('auth/login', 'refresh'); }
        
    }
	
	public function index()
	{
        $data['judul'] = 'Profil';
        $this->template->display('v_profil',$data);
    }

    public function update()
	{
		$status = 'gagal';
		$username = $this->input->post('username');
		$nama = $this->input->post('nama');
		$password = $this->input->post('password');
		$datau = array(
            'username'=>$username,
            'first_name' => $nama
        );
		
		if ($this->input->post('password')!='') { 
		$datau['password'] = $this->input->post('password');
		}
		$masuk = $this->ion_auth->update($this->ion_auth->user()->row()->id, $datau);
		$status = 'berhasil';
		$hasil = array('status'=>$status);
		echo json_encode($hasil);

	}

    
    
    


}
