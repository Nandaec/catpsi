<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'third_party/Spout/Autoloader/autoload.php';

class Expor extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        //$this->load->library('pdf');
    }
    public function index()
    { }
    /*
    public function ujian()
    {
        $idujian = $this->uri->segment(3);
        $ujian = $this->model_kueri->kueri_row_array('ujian',array('id_ujian'=>$idujian));
        $jumlah_kolom = $ujian->jumlah_kolom;
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->setPrintFooter(false);
        $pdf->setPrintHeader(false);
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
        
        $pdf->Write(0, strtoupper($ujian->nama_ujian), '', 0, 'C', true, 0, false, false, 0);
        $pdf->SetFont('');
        $kolomstart = 1;
        for ($offset=0;($offset+4)<=$jumlah_kolom;$offset++)
        {
            //$kolom = $offset + 1;
            $pdf->AddPage('');
            $pdf->SetFont('times', '', 10);
            $pdf->Write(0, strtoupper($ujian->nama_ujian).' '.$jumlah_kolom.' '.$offset, '', 0, 'C', true, 0, false, false, 0);
            $tabelkolom = '<table cellspacing="6" border="0">';
            $tabelkolom .= '<tr>';
            //$getkolom = $this->db->query("SELECT * FROM ")
            //$kolom = $
            for ($kolom=$kolomstart;$kolom<=($kolomstart+3);$kolom++ )
            {
                $tabelkolom .= '<td>';
                $qtabel = $this->db->query("SELECT * FROM ujian_kolom WHERE ujian_id = '$idujian' AND kolom = '$kolom' ORDER BY huruf ASC");
                if ($qtabel->num_rows() > 0)
                {   $tabelkolom .= '<table border="1" cellpadding="2">';
                    $tabelkolom .= '<tr><td colspan="5" style="text-align:center;">Kolom '.$kolom.'</td></tr>';
                    $tabelkolom .= '<tr>';
                    foreach ($qtabel->result() as $qt){
                        $tabelkolom .= '<td style="text-align:center;">'.$qt->huruf.'</td>';
                    }
                    $tabelkolom .= '</tr>';
                    $tabelkolom .= '<tr>';
                    foreach ($qtabel->result() as $qt){
                        $tabelkolom .= '<td style="text-align:center;">'.$qt->kolom_nilai.'</td>';
                    }
                    $tabelkolom .= '</tr>';
                    $tabelkolom .= '</table>';
                }
                $tabelkolom .= '</td>';
            }
            $tabelkolom .= '</tr>';
            $tabelkolom .= '<tr><td></td></tr>';
            // SOAL
            $tabelkolom .= '<tr>';
            for ($kolom=$kolomstart;$kolom<=($kolomstart+3);$kolom++ ){
            $tabelkolom .= '<td>';
            // SOAL DISINI
            $qsoal = $this->db->query("SELECT * FROM ujian_soal WHERE ujian_id = '$idujian' AND kolom = '$kolom' ORDER BY soal_ke ASC");
            if ($qsoal->num_rows() > 0)
            {
                foreach ($qsoal->result() as $soal)
                {
                    $tabelkolom .= '<table border="1" cellpadding="2"><tr>';
                    $tabelkolom .= '<td style="text-align:center;">'.$soal->soal_ke.'</td>';
                    $tabelkolom .= '<td style="text-align:center;">'.$soal->huruf_1.'</td>';
                    $tabelkolom .= '<td style="text-align:center;">'.$soal->huruf_2.'</td>';
                    $tabelkolom .= '<td style="text-align:center;">'.$soal->huruf_3.'</td>';
                    $tabelkolom .= '<td style="text-align:center;">'.$soal->huruf_4.'</td>';
                    $tabelkolom .= '<td style="text-align:center;">'.$soal->jawaban.'</td>';
                    $tabelkolom .= '</tr></table>';
                }
            }
            // SOAL DISINI
            $tabelkolom .= '</td>';
            }
            $tabelkolom .= '</tr>';
            // SOAL

            $tabelkolom .= '</table>';
            $kolomstart = $kolomstart + 4;
            //$tabelkolom ='<table cellspacing="0" cellpadding="2" border="1">';
            //$tabelkolom .= '<tr><td>Kolom '.$offset.'</td></tr>';
            //$tabelkolom .= '</table>';
            $pdf->writeHTML($tabelkolom);

        }
        
        
        //$pdf->writeHTML($tabel);
        $pdf->Output('file-pdf-codeigniter.pdf', 'I');
    } 
	*/

    public function ujian_soal()
    {
        $idujian = $this->uri->segment(3);
        $ujian = $this->model_kueri->kueri_row_array('ujian',array('id_ujian'=>$idujian));
        $data['idujian'] = $idujian;
        $data['judul'] = strtoupper($ujian->nama_ujian);
        $this->load->view('v_export',$data);
    }
    
    public function hasil()
    {
        $idujian = $this->uri->segment(3);
        $idpeserta = $this->uri->segment(4);
        $data['namapeserta'] = $this->model_kueri->return_kolom_array('first_name','users',array('id'=>$idpeserta));
        $ujian = $this->model_kueri->kueri_row_array('ujian',array('id_ujian'=>$idujian));
        $data['idujian'] = $idujian;
        $data['idpeserta'] = $idpeserta;

        $cek = $this->db->query("SELECT * FROM ujian WHERE id_ujian='$idujian'")->row();
        if($ujian->jenis == 'ujian'){
            $hasilnya = $this->model_kueri->hasil_tes($idpeserta,$idujian);
            $data['judul'] = 'HASIL TES CAT PSIKOLOGI '.strtoupper($data['namapeserta']).' : '.$hasilnya;
        }else{
            $hasilnya = $this->db->query("SELECT sum(jumlah_benar) as jumlah_benar FROM peserta_hasil WHERE ujian_id='$idujian' AND peserta_id='$idpeserta'")->row('jumlah_benar');
            $data['judul'] = 'HASIL TES CAT PSIKOLOGI '.strtoupper($data['namapeserta']).' : '.$hasilnya;
        }
        $this->load->view('v_export_hasil',$data);
    }
    
    


}
