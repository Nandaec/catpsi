<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peserta extends CI_Controller {
    function __construct(){
        parent::__construct();
        if (!$this->ion_auth->logged_in()) { redirect('auth/login', 'refresh'); }
        if (!$this->ion_auth->in_group(2)) { redirect('dashboard', 'refresh'); }
        
    }
	
	public function index()
	{
        $data['judul'] = 'Welcome '.$this->ion_auth->user()->row()->first_name;    
        $idpeserta = $this->ion_auth->user()->row()->id;
			$data['home'] = $this->model_kueri->kueri_row_array('home',array('id_home'=>1));
			$ujian = $this->db->query("SELECT a.id_ujian,a.tanggal,a.keterangan FROM ujian a JOIN peserta b ON a.kelas_id = b.kelas_id WHERE a.aktif='Y' AND b.peserta_id = '$idpeserta'")->row();
			$idujian = $ujian->id_ujian;
			$data['idujian'] = $idujian;
			$data['idpeserta'] = $idpeserta;
			$data['tanggalujian'] = $ujian->tanggal;
            $data['keterangan'] = $ujian->keterangan;
			$data['ceksudah'] = $this->model_kueri->cek_jumlah_array_minimal('id_peserta_ujian','peserta_ujian',array('peserta_id'=>$idpeserta,'ujian_id'=>$idujian,'status'=>'Y'));
			$this->template->display('v_dashboard_peserta',$data);
    }

    public function mulai()
	{
        $data['judul'] = 'Selamat Mengerjakan';
        $idpeserta = $this->ion_auth->user()->row()->id;
        $ujian = $this->db->query("SELECT a.id_ujian,a.tanggal,a.jumlah_kolom,a.jenis FROM ujian a JOIN peserta_ujian b ON a.id_ujian = b.ujian_id WHERE  b.peserta_id = '$idpeserta'")->row();
        $idujian = $ujian->id_ujian;
        $ceksudah = $this->model_kueri->cek_jumlah_array_minimal('id_peserta_ujian','peserta_ujian',array('peserta_id'=>$idpeserta,'ujian_id'=>$idujian,'status'=>'Y'));
        if ($ceksudah > 0) { redirect ('dashboard','refresh'); }
        $data['idujian'] = $idujian;
        $data['idpeserta'] = $idpeserta;
        $data['tanggalujian'] = $ujian->tanggal;
        $data['jenis'] = $ujian->jenis;
        $data['makskolom'] = intval($ujian->jumlah_kolom);
        $this->session->set_userdata('soalke', 1);
        if($ujian->jenis == 'ujian'){
            $this->template->display('v_mulai',$data);
        }else{
            $this->template->display('v_mulai_koran',$data);
        }
        

    }

    public function get_soal()
    {
        $idpeserta = $this->ion_auth->user()->row()->id;
        $kolom = intval($this->input->post('kolom'));
        $ujian = $this->db->query("SELECT a.id_ujian,a.tanggal,a.durasi_kolom,a.durasi_detik,a.jumlah_kolom,a.keterangan FROM ujian a JOIN peserta_ujian b ON a.id_ujian = b.ujian_id WHERE b.peserta_id = '$idpeserta'")->row();
        $idujian = $ujian->id_ujian;
        $data['jawaban_peserta'] = $this->model_kueri->return_kolom_array('jawaban_peserta','peserta_hasil',array('peserta_id'=>$idpeserta,'ujian_id'=>$idujian,'kolom_soal'=>$kolom));
        $data['ujikolom'] = $this->db->query("SELECT * FROM ujian_kolom WHERE ujian_id = '$idujian' AND kolom = '$kolom' ORDER BY huruf ASC")->result();
        $data['kolom'] = $kolom;
        $data['durasi'] = $ujian->durasi_kolom;
        $data['detik'] = $ujian->durasi_detik;
        $data['soals'] = $this->db->query("SELECT * FROM ujian_soal WHERE ujian_id = '$idujian' AND kolom = '$kolom' ORDER BY soal_ke ASC");
        $data['jumsoal'] = $data['soals']->num_rows();
        $data['idujian'] = $idujian;
        $this->session->set_userdata('soalke', $kolom);
        if($ujian->keterangan == 'Tampil semua') {
            $this->load->view('v_soal',$data);
        } else {
            $this->load->view('v_soal_dua',$data);
        }
    }

    public function get_soal_koran()
    {
        $idpeserta = $this->ion_auth->user()->row()->id;
        $kolom = intval($this->input->post('kolom'));
        $ujian = $this->db->query("SELECT a.id_ujian,a.tanggal,a.durasi_kolom,a.durasi_detik,a.jumlah_kolom,a.keterangan FROM ujian a JOIN peserta_ujian b ON a.id_ujian = b.ujian_id WHERE b.peserta_id = '$idpeserta'")->row();
        $idujian = $ujian->id_ujian;
        $data['jawaban_peserta'] = $this->model_kueri->return_kolom_array('jawaban_peserta','peserta_hasil',array('peserta_id'=>$idpeserta,'ujian_id'=>$idujian,'kolom_soal'=>$kolom));

        $data['kolom'] = $kolom;
        $data['durasi'] = $ujian->durasi_kolom;
        $data['detik'] = $ujian->durasi_detik;
        $data['s'] = $this->db->query("SELECT * FROM koran_ujian WHERE ujian_id = '$idujian' AND kolom = '$kolom' ORDER BY kolom ASC")->row();
        $data['soals'] = $this->db->query("SELECT * FROM koran_ujian WHERE ujian_id = '$idujian' AND kolom = '$kolom' ORDER BY kolom ASC");
        $data['jumsoal'] = $data['soals']->num_rows();
        $data['idujian'] = $idujian;
        $this->session->set_userdata('soalke', $kolom);
        if($ujian->keterangan == 'Tampil semua') {
            $this->load->view('v_soal_koran',$data);
        } else {
            $this->load->view('v_soal_koran',$data);
        }
    }

    // public function ambil_soal()
    // {
    //     $id_ujian   = $this->input->post('id_ujian');
    //     $kolom      = $this->input->post('kolom');
        
    //     $data = $this->db->query("SELECT * FROM koran_ujian WHERE ujian_id='$id_ujian' AND kolom='$kolom'")->result();
    //     echo json_encode(['data' => $data]);
    // }

    public function simpan_jawaban()
    {
        $idpeserta = $this->ion_auth->user()->row()->id;
        $idujian = $this->input->post('idujian');
        $jumsoal = $this->input->post('jumsoal');
        $kolom = $this->input->post('kolomujian');
        $soal_dijawab = '';
        $jawaban_peserta = '';
        $jumlah_benar = 0;
        for ($i=1;$i<=$jumsoal;$i++)
        {
            $jawaban_benar = $this->model_kueri->return_kolom_array('jawaban','ujian_soal',array('ujian_id'=>$idujian,'kolom'=>$kolom,'soal_ke'=>$i));
            $soalke =  ($this->input->post('soalke'.$i)!= '') ? $this->input->post('soalke'.$i) : $i;
            $jawabke =  $this->input->post('jawabke'.$i);
            $jawabke = ($jawabke != '') ? $jawabke : 'NO';
            //if ($soalke != '') {$soal_dijawab .= $soalke.','; }
            //if ($jawabke != '') {$jawaban_peserta .= $soalke.';'.$jawabke.','; }
            $soal_dijawab .= $soalke.',';
            $jawaban_peserta .= $soalke.';'.$jawabke.',';
            if ($jawabke === $jawaban_benar) { $jumlah_benar = $jumlah_benar + 1;}
            
        }
        $this->db->set('soal_dijawab', $soal_dijawab);
        $this->db->set('jawaban_peserta', $jawaban_peserta);
        $this->db->set('jumlah_benar', $jumlah_benar);
        $this->db->where('peserta_id', $idpeserta);
        $this->db->where('ujian_id', $idujian);
        $this->db->where('kolom_soal', $kolom);
        $this->db->update('peserta_hasil');
        $hasil = array('idujian'=>$idujian,'id_peserta'=>$idpeserta,'kolom'=>$kolom);
        echo json_encode($hasil);
    }
    
    public function simpan_jawaban_koran()
    {
        $idpeserta  = $this->ion_auth->user()->row()->id;
        $idujian    = $this->input->post('id_ujian');
        $jumsoal    = $this->input->post('jumsoal');
        $kolom      = $this->input->post('kolomujian');
        $tot_baris  = $this->input->post('tot_baris');
        $baris      = $this->input->post('baris');
        $jawaban    = $this->input->post('inp_jawab');

        $soal_dijawab = '';
        $jawaban_peserta = '';
        $jumlah_benar = 0;

        $no=1;
        $jwb=19;
        foreach($jawaban AS $key => $val){
            $jawaban_benar = $this->model_kueri->return_kolom_array('s'.$no,'koran_jawaban',array('ujian_id'=>$idujian,'kolom'=>$kolom));
            $soalke =  $no;
            $hehe =  $_POST['inp_jawab'][$jwb];

            $pjg = strlen($hehe);
            $jawabke = substr($hehe,-1,$pjg);

            $jawabke = ($jawabke != '') ? $jawabke : 'NO';
            $soal_dijawab .= $soalke.',';
            $jawaban_peserta .= $soalke.';'.$jawabke.',';
            if ($jawabke === $jawaban_benar) { 
                $jumlah_benar = $jumlah_benar + 1;
            }
            $no++;
            $jwb--;
        }
        $this->db->set('soal_dijawab', $soal_dijawab);
        $this->db->set('jawaban_peserta', $jawaban_peserta);
        $this->db->set('jumlah_benar', $jumlah_benar);
        $this->db->where('peserta_id', $idpeserta);
        $this->db->where('ujian_id', $idujian);
        $this->db->where('kolom_soal', $kolom);
        $this->db->update('peserta_hasil');
        $hasil = array('idujian'=>$idujian,'id_peserta'=>$idpeserta,'kolom'=>$kolom);
        echo json_encode($hasil);
    }

    public function selesai()
    {
        $a = array('status'=>'Y');
        $idpeserta = $this->ion_auth->user()->row()->id;
        $ujian = $this->db->query("SELECT a.id_ujian,a.tanggal,a.durasi_kolom,a.jumlah_kolom,a.jenis FROM ujian a JOIN peserta_ujian b ON a.id_ujian = b.ujian_id WHERE b.peserta_id = '$idpeserta'")->row();
        $idujian = $ujian->id_ujian;
        $data['jumlah_kolom'] = $ujian->jumlah_kolom;
        $data['peserta_hasil'] = $this->model_kueri->pilih_array_order('peserta_hasil',array('peserta_id'=>$idpeserta,'ujian_id'=>$idujian),'kolom_soal','ASC');
        $this->model_utama->update('peserta_ujian',$a,array('peserta_id'=>$idpeserta,'ujian_id'=>$idujian));
        $data['idujian'] = $idujian;
        $data['idpeserta'] = $idpeserta;

        if($ujian->jenis == 'ujian'){
            $hasiltes = $this->model_kueri->hasil_tes($idpeserta,$idujian);
        }else{
            $hasiltes = $this->db->query("SELECT sum(jumlah_benar) as jumlah_benar FROM peserta_hasil WHERE ujian_id='$idujian' AND peserta_id='$idpeserta'")->row('jumlah_benar');
        }
        $data['judul'] = 'Hasil Tes '.$this->ion_auth->user()->row()->first_name.' : '.$hasiltes;
        if($ujian->jenis == 'ujian'){
            $this->template->display('v_selesai',$data);
        }else{
            $this->template->display('v_selesai_koran',$data);
        }
    }

    

    
    


}
