<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    function __construct(){
        parent::__construct();
        if (!$this->ion_auth->logged_in()) { redirect('auth/login', 'refresh'); }
        if (!$this->ion_auth->in_group(1)) { redirect('dashboard', 'refresh'); }
        
    }
	
	public function index()
	{
        $data['judul'] = 'Selamat Datang ';
        $this->template->display('v_beranda',$data);
    }

    public function home()
	{
        $data['judul'] = 'Pesan dan Video Home ';
        $data['home'] = $this->model_kueri->kueri_row_array('home',array('id_home'=>1));
        $data['home2'] = $this->model_kueri->kueri_row_array('home',array('id_home'=>2));
        $this->template->display('v_home',$data);
    }

    public function update_pesan()
    {
        $pesan = $this->input->post('pesan');
        $status = 'gagal';
        $up = $this->model_utama->update('home',array('pesan'=>$pesan),array('id_home'=>1));
        if ($up) { $status = 'berhasil'; }
		    $hasil = array('status'=>$status);
			echo json_encode($hasil);   
    }

    public function update_pesan2()
    {
        $pesan = $this->input->post('pesan');
        $status = 'gagal';
        $up = $this->model_utama->update('home',array('pesan'=>$pesan),array('id_home'=>2));
        if ($up) { $status = 'berhasil'; }
            $hasil = array('status'=>$status);
            echo json_encode($hasil);   
    }

    public function update_video()
    {
        $config['upload_path']          = './assets/';
        $config['allowed_types'] = 'wmv|mp4|avi|mov';
        $config['max_size'] = '0';
        $config['max_filename'] = '255';
        $config['encrypt_name'] = FALSE;
        $this->load->library('upload', $config);
        $this->upload->do_upload('video_name');
        $hasil=$this->upload->data();
        $up = $this->model_utama->update('home',array('video'=>$hasil['file_name']),array('id_home'=>1));
        redirect('admin/home');
    }
    

    public function user()
	{
        $data['judul'] = 'Data User';
        $this->template->display('v_user',$data);
    }

    public function user_list()
	{
		$this->load->model('model_user');
		$list = $this->model_user->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$No = 1;
		
		foreach ($list as $kecs) {
            $status = ($kecs->active == 1) ? 'Aktif' : 'Tidak Aktif';
            $no++;
		    $row = array();
			$row[] = $kecs->first_name;
            $row[] = $kecs->username;
            $row[] = $kecs->pwd;
            $row[] = $status;
            $row[] = '<div class="text-center">
                      <div class="btn-group" role="group" aria-label="Action">
                            <button type="button" title="Edit" class="btn btn-sm btn-success" onclick="edit_user('.$kecs->iduser.')"><i class="fa fa-edit"></i></button>
                            <button type="button" title="Hapus" class="btn btn-sm btn-danger" onclick="delete_user('.$kecs->iduser.')"><i class="fa fa-times"></i></button>
                      </div>
                      </div>';
		
			$data[] = $row;
			$No++;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_user->count_all(),
						"recordsFiltered" => $this->model_user->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
		
	}
    
    public function user_add()
	{
			$status = 'gagal';
			$nama = $this->input->post('nama',TRUE);
            $username = $this->input->post('username',TRUE);
            $aktif = $this->input->post('status');
            $password = ($this->input->post('password')!= '') ? $this->input->post('password',TRUE) : $this->input->post('username',TRUE);
            $ab = strtolower($nama);
            $ab = str_replace(' ','',$ab);
            $email = $ab.'@gmail.com'; 
            $additional_data = array(
                'first_name' => $nama,
                'pwd'=>$password
                );
            $group = array('2'); 
            $masuk = $this->ion_auth->register($username, $password, $email, $additional_data, $group);
			if ($masuk) { $status = 'berhasil'; }
		    $hasil = array('status'=>$status);
			echo json_encode($hasil);            			
		    
    }
    
    public function user_edit($id)
	{
		$this->load->model('model_user');
		$data = $this->model_user->get_by_id($id);
		echo json_encode($data);
    }
    
    public function user_update()
	{
			$status = 'gagal';
			$id = $this->input->post('id');
			$username = $this->input->post('username');
            $nama = $this->input->post('nama',TRUE);
            $password = $this->input->post('password',TRUE);
            $status = $this->input->post('status');
            if ($password == '')
            {
                $data = array('first_name'=>$nama,'username'=>$username,'active'=>$status);
               
            } else {
                $data = array('first_name'=>$nama,'username'=>$username,'password'=>$password,'active'=>$status,'pwd'=>$password);
                
            }
            $masuk = $this->ion_auth->update($id, $data);
			if ($masuk) { $status = 'berhasil'; }
			    $hasil = array('status'=>$status);
			echo json_encode($hasil);
		
    }
    
    public function user_delete()
	{
        $status = 'gagal';    
        $masuk = $this->ion_auth->delete_user($this->input->post('iddelete'));
        if ($masuk) { $status = 'berhasil'; }
        $hasil = array('status'=>$status);
        echo json_encode($hasil);	
		
    }

    public function kelas()
	{
        $data['judul'] = 'Data Kelas';
        $this->template->display('v_kelas',$data);
    }

    public function kelas_list()
	{
		$this->load->model('model_kelas');
		$list = $this->model_kelas->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$No = 1;
		
		foreach ($list as $kecs) {
            $no++;
            $jumpeserta = $this->model_kueri->cek_jumlah_array_minimal('id_peserta','peserta',array('kelas_id'=>$kecs->id_kelas));
		    $row = array();
			$row[] = '<div class="text-center">'.$no.'</div>';
			$row[] = $kecs->nama_kelas;
            $row[] = $kecs->keterangan;
            $row[] = '<div class="text-center"><a href="'.site_url('admin/peserta_kelas/'.$kecs->id_kelas).'">'.$jumpeserta.' Peserta</a></div>';
			$row[] = '<div class="text-center">
			<a class="btn btn-sm btn-success" href="javascript:void(0)" title="EDIT" onclick="edit_data('.$kecs->id_kelas.')"><i class="fa fa-edit"></i></a>		  
			<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="HAPUS" onclick="konfirm_hapus('.$kecs->id_kelas.')"><i class="fa fa-times"></i></a>
			</div>
			';
		
			$data[] = $row;
			$No++;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_kelas->count_all(),
						"recordsFiltered" => $this->model_kelas->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
		
    }
    

    
    public function kelas_add()
	{
        $this->load->model('model_kelas');
        $status = 'gagal';
        $nama = $this->input->post('nama');
        $keterangan = $this->input->post('keterangan');
        $data = array('nama_kelas'=>$nama,'keterangan'=>$keterangan);
        $masuk = $this->model_kelas->save($data);
        if ($masuk) { $status = 'berhasil'; }
        $hasil = array('status'=>$status);
        echo json_encode($hasil);         			
		    
    }
    
    public function kelas_edit($id)
	{
		$this->load->model('model_kelas');
		$data = $this->model_kelas->get_by_id($id);
		echo json_encode($data);
    }
    
    public function kelas_update()
	{
        $this->load->model('model_kelas');
        $status = 'gagal';
        $id = $this->input->post('id');
        $nama = $this->input->post('nama');
        $keterangan = $this->input->post('keterangan');
        $data = array('nama_kelas'=>$nama,'keterangan'=>$keterangan);
        $masuk = $this->model_kelas->update(array('id_kelas'=>$id),$data);
        if ($masuk) { $status = 'berhasil'; }
        $hasil = array('status'=>$status);
        echo json_encode($hasil);
		
    }
    
    public function kelas_delete()
	{
        $status = 'gagal';
        $id = $this->input->post('iddelete');
        $peserta = $this->db->where('kelas_id',$id);
        $peserta = $this->db->delete('peserta');
        $masuk = $this->db->where('id_kelas',$id);
        $masuk = $this->db->delete('kelas');
        if ($masuk) { $status = 'berhasil'; }
        $hasil = array('status'=>$status);
        echo json_encode($hasil);	
		
    }

    public function peserta_kelas()
	{
        $idkelas= $this->uri->segment(3);
        $namakelas = $this->model_kueri->return_kolom_array('nama_kelas','kelas',array('id_kelas'=>$idkelas));
        $data['judul'] = 'Data Peserta Kelas '.$namakelas;
        $data['idkelas'] = $idkelas;
        $this->template->display('v_peserta_kelas',$data);
    }

    public function get_peserta_kelas()
    {
       //$idkelas = $this->input->post('idkelas');
       $ps = $this->db->query("SELECT a.id,a.first_name FROM users a JOIN users_groups b ON a.id = b.user_id WHERE a.id NOT IN (SELECT peserta_id FROM peserta) AND b.group_id = 2");
       if ($ps->num_rows() > 0)
       {
           foreach ($ps->result() as $p) {
               echo "<option value='".$p->id."'>".$p->first_name."</option>";
           } 
       } else {
        echo "<option value=''>Tidak ada data user</option>";
       }
    }

    public function peserta_kelas_list($idkelas)
	{
		$this->load->model('model_peserta_kelas');
		$list = $this->model_peserta_kelas->get_datatables($idkelas);
		$data = array();
		$no = $_POST['start'];
		$No = 1;
		
		foreach ($list as $kecs) {
            $no++;
            $row = array();
			$row[] = '<div class="text-center">'.$no.'</div>';
			$row[] = $kecs->username;
            $row[] = $kecs->first_name;
            $row[] = '<div class="text-center"><a class="btn btn-sm btn-danger" href="javascript:void(0)" title="HAPUS" onclick="konfirm_hapus('.$kecs->peserta_id.')"><i class="fa fa-times"></i></a></div>';
		
			$data[] = $row;
			$No++;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_peserta_kelas->count_all($idkelas),
						"recordsFiltered" => $this->model_peserta_kelas->count_filtered($idkelas),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
		
    }

    public function peserta_kelas_add()
	{
        $this->load->model('model_peserta_kelas');
        $status = 'gagal';
        $idkelas = $this->input->post('idkelas');
        $peserta = $this->input->post('allpeserta');
        //$pecah = explode(',',$peserta);
        $jum = count($peserta);
        for ($a=0;$a<$jum;$a++)
        {    $data = array('kelas_id'=>$idkelas,'peserta_id'=>$peserta[$a]);
             $masuk = $this->model_peserta_kelas->save($data);

        }
        
        if ($masuk) { $status = 'berhasil'; }
        $hasil = array('status'=>$status);
        echo json_encode($hasil);         			
		    
    }

    public function peserta_kelas_delete()
	{
        $status = 'gagal';
        $id = $this->input->post('iddelete');
        $masuk = $this->db->where('peserta_id',$id);
        $masuk = $this->db->delete('peserta');
        if ($masuk) { $status = 'berhasil'; }
        $hasil = array('status'=>$status);
        echo json_encode($hasil);	
		
    }

    public function peserta()
	{
        $idujian = $this->uri->segment(3);
        $namaujian = $this->model_kueri->return_kolom_array('nama_ujian','ujian',array('id_ujian'=>$idujian));
        $data['judul'] = 'Data Peserta '.$namaujian;
        $data['idujian'] = $idujian;
        $this->template->display('v_peserta',$data);
    }

    public function get_peserta()
    {
       //$idkelas = $this->input->post('idkelas');
       $ps = $this->db->query("SELECT a.id,a.first_name FROM users a JOIN users_groups b ON a.id = b.user_id WHERE a.id NOT IN (SELECT peserta_id FROM peserta_ujian) AND b.group_id = 2");
       if ($ps->num_rows() > 0)
       {
           foreach ($ps->result() as $p) {
               echo "<option value='".$p->id."'>".$p->first_name."</option>";
           } 
       } else {
        echo "<option value=''>Tidak ada data user</option>";
       }
    }

    public function peserta_list($idujian)
	{
		$this->load->model('model_peserta');
		$list = $this->model_peserta->get_datatables($idujian);
		$data = array();
		$no = $_POST['start'];
		$No = 1;
		
		foreach ($list as $kecs) {
            $btndelete = ($kecs->status == 'N') ? '<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="HAPUS" onclick="konfirm_hapus('.$kecs->peserta_id.')"><i class="fa fa-times"></i></a>' : 'Sudah Mengikuti Tes';
            $no++;
            $row = array();
			$row[] = '<div class="text-center">'.$no.'</div>';
			$row[] = $kecs->username;
            $row[] = $kecs->first_name;
            $row[] = '<div class="text-center">'.$btndelete.'</div>';
		
			$data[] = $row;
			$No++;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_peserta->count_all($idujian),
						"recordsFiltered" => $this->model_peserta->count_filtered($idujian),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
		
    }

    public function peserta_add()
	{
        $this->load->model('model_peserta');
        $status = 'gagal';
        $idujian = $this->input->post('idujian');
        $peserta = $this->input->post('allpeserta');
        //$pecah = explode(',',$peserta);
        $jum = count($peserta);
        for ($a=0;$a<$jum;$a++)
        {    $data = array('ujian_id'=>$idujian,'peserta_id'=>$peserta[$a]);
             $masuk = $this->model_peserta->save($data);

        }
        
        if ($masuk) { $status = 'berhasil'; }
        $hasil = array('status'=>$status);
        echo json_encode($hasil);         			
		    
    }

    public function peserta_delete()
	{
        $status = 'gagal';
        $id = $this->input->post('iddelete');
        $masuk = $this->db->where('peserta_id',$id);
        $masuk = $this->db->delete('peserta_ujian');
        if ($masuk) { $status = 'berhasil'; }
        $hasil = array('status'=>$status);
        echo json_encode($hasil);	
		
    }

    public function ujian()
	{
        $data['judul'] = 'Data Ujian';
        $data['kelas'] = $this->model_kueri->get_array('kelas');
        $this->template->display('v_ujian',$data);
    }

    public function ujian_koran()
    {
        $data['judul'] = 'Data Ujian';
        $data['kelas'] = $this->model_kueri->get_array('kelas');
        $this->template->display('v_ujian_koran',$data);
    }

    public function ujian_list()
	{
		$this->load->model('model_ujian');
		$list = $this->model_ujian->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$No = 1;
		
		foreach ($list as $kecs) {
            $no++;
            $tblaktif = ($kecs->aktif == 'N') ? '<a class="btn btn-sm btn-warning" href="javascript:void(0)" title="Aktifkan Ujian" onclick="aktifkan('.$kecs->id_ujian.')"><i class="fa fa-check"></i></a>' : "";
		    $jumpeserta = $this->model_kueri->cek_jumlah_array_minimal('id_peserta_ujian','peserta_ujian',array('ujian_id'=>$kecs->id_ujian));
            $row = array();
			$row[] = '<div class="text-center">'.$no.'</div>';
            //$row[] = '<a href="'.site_url('admin/ujian_detail/'.$kecs->id_ujian).'">'.$kecs->nama_kelas.'</a>';
            $row[] = '<a href="'.site_url('admin/ujian_detail/'.$kecs->id_ujian).'">'.$kecs->nama_ujian.'</a>';
            $row[] = $this->mylibrary->tgl_indo($kecs->tanggal);
            $row[] = $kecs->jumlah_kolom;
            $row[] = $kecs->durasi_kolom.' menit '.$kecs->durasi_detik.' detik';
            $row[] = $kecs->keterangan;
            $row[] = '<div class="text-center"><a href="'.site_url('admin/peserta/'.$kecs->id_ujian).'">'.$jumpeserta.' Peserta</a></div>';
            $row[] = '<div class="text-center">
            <a class="btn btn-sm btn-primary" href="'.site_url('expor/ujian_soal/'.$kecs->id_ujian).'" title="Export Soal"><i class="fa fa-file-pdf-o"></i></a>
            <a class="btn btn-sm btn-info" href="'.site_url('admin/ujian_kolom/'.$kecs->id_ujian).'" title="Buat Soal"><i class="fa fa-book"></i></a>
            <a class="btn btn-sm btn-success" href="javascript:void(0)" title="EDIT" onclick="edit_data('.$kecs->id_ujian.')"><i class="fa fa-edit"></i></a>		  
			<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="HAPUS" onclick="konfirm_hapus('.$kecs->id_ujian.')"><i class="fa fa-times"></i></a>
			</div>
			';
		
			$data[] = $row;
			$No++;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_ujian->count_all(),
						"recordsFiltered" => $this->model_ujian->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);		
    }
    
    public function ujian_list_koran()
    {
        $this->load->model('model_ujian');
        $list = $this->model_ujian->get_datatables_koran();
        $data = array();
        $no = $_POST['start'];
        $No = 1;
        
        foreach ($list as $kecs) {
            $no++;
            $tblaktif = ($kecs->aktif == 'N') ? '<a class="btn btn-sm btn-warning" href="javascript:void(0)" title="Aktifkan Ujian" onclick="aktifkan('.$kecs->id_ujian.')"><i class="fa fa-check"></i></a>' : "";
            $jumpeserta = $this->model_kueri->cek_jumlah_array_minimal('id_peserta_ujian','peserta_ujian',array('ujian_id'=>$kecs->id_ujian));
            $row = array();
            $row[] = '<div class="text-center">'.$no.'</div>';
            $row[] = '<a href="'.site_url('admin/ujian_detail/'.$kecs->id_ujian).'">'.$kecs->nama_ujian.'</a>';
            $row[] = $this->mylibrary->tgl_indo($kecs->tanggal);
            $row[] = $kecs->jumlah_kolom;
            $row[] = $kecs->durasi_kolom.' menit '.$kecs->durasi_detik.' detik';
            $row[] = $kecs->keterangan;
            $row[] = '<div class="text-center"><a href="'.site_url('admin/peserta/'.$kecs->id_ujian).'">'.$jumpeserta.' Peserta</a></div>';
            $row[] = '<div class="text-center">
            <a class="btn btn-sm btn-primary" href="'.site_url('expor/ujian_soal/'.$kecs->id_ujian).'" title="Export Soal"><i class="fa fa-file-pdf-o"></i></a>
            <a class="btn btn-sm btn-info" href="'.site_url('admin/ujian_kolom_koran/'.$kecs->id_ujian).'" title="Buat Soal"><i class="fa fa-book"></i></a>
            <a class="btn btn-sm btn-success" href="javascript:void(0)" title="EDIT" onclick="edit_data('.$kecs->id_ujian.')"><i class="fa fa-edit"></i></a>          
            <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="HAPUS" onclick="konfirm_hapus('.$kecs->id_ujian.')"><i class="fa fa-times"></i></a>
            </div>
            ';
        
            $data[] = $row;
            $No++;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model_ujian->count_all_koran(),
            "recordsFiltered" => $this->model_ujian->count_filtered_koran(),
            "data" => $data,
        );

        echo json_encode($output);      
    }
    
    public function ujian_add()
	{
        $this->load->model('model_ujian');
        $status = 'gagal';
        $kelas = $this->input->post('kelas');
        $nama = $this->input->post('nama');
        $tanggal = date('Y-m-d',strtotime($this->input->post('tanggal')));
        $jumlah = $this->input->post('jumlah');
        $durasi = $this->input->post('durasi');
        $detik = $this->input->post('detik');
        $keterangan = $this->input->post('keterangan');
        $jenis = 'ujian';
        $data = array('kelas_id'=>$kelas,'nama_ujian'=>$nama,'tanggal'=>$tanggal,'jumlah_kolom'=>$jumlah,'durasi_kolom'=>$durasi,'durasi_detik'=>$detik,'keterangan'=>$keterangan,'jenis'=>$jenis);
        $masuk = $this->model_ujian->save($data);
        if ($masuk) { $status = 'berhasil'; }
        $hasil = array('status'=>$status);
        echo json_encode($hasil);         			
		    
    }

    public function ujian_add_koran()
    {
        $this->load->model('model_ujian');
        $status = 'gagal';
        $kelas = $this->input->post('kelas');
        $nama = $this->input->post('nama');
        $tanggal = date('Y-m-d',strtotime($this->input->post('tanggal')));
        $jumlah = $this->input->post('jumlah');
        $durasi = $this->input->post('durasi');
        $detik = $this->input->post('detik');
        $keterangan = 'Tampil per soal';
        // $keterangan = $this->input->post('keterangan');
        $jenis = 'koran';
        $data = array('kelas_id'=>$kelas,'nama_ujian'=>$nama,'tanggal'=>$tanggal,'jumlah_kolom'=>$jumlah,'durasi_kolom'=>$durasi,'durasi_detik'=>$detik,'keterangan'=>$keterangan,'jenis'=>$jenis);
        $masuk = $this->model_ujian->save($data);
        if ($masuk) { $status = 'berhasil'; }
        $hasil = array('status'=>$status);
        echo json_encode($hasil);                   
            
    }
    
    public function ujian_edit($id)
	{
		$this->load->model('model_ujian');
		$data = $this->model_ujian->get_by_id($id);
		echo json_encode($data);
    }
    
    public function ujian_update()
	{
        $this->load->model('model_ujian');
        $status = 'gagal';
        $id = $this->input->post('id');
        $kelas = $this->input->post('kelas');
        $nama = $this->input->post('nama');
        $tanggal = date('Y-m-d',strtotime($this->input->post('tanggal')));
        $jumlah = $this->input->post('jumlah');
        $durasi = $this->input->post('durasi');
        $detik = $this->input->post('detik');
        $keterangan = $this->input->post('keterangan');
        $data = array('kelas_id'=>$kelas,'nama_ujian'=>$nama,'tanggal'=>$tanggal,'jumlah_kolom'=>$jumlah,'durasi_kolom'=>$durasi,'durasi_detik'=>$detik,'keterangan'=>$keterangan);
        $masuk = $this->model_ujian->update(array('id_ujian'=>$id),$data);
        if ($masuk) { $status = 'berhasil'; }
        $hasil = array('status'=>$status);
        echo json_encode($hasil);
		
    }
    
    public function ujian_delete()
	{
        $status = 'gagal';
        $id = $this->input->post('iddelete');
        $masuk = $this->db->where('id_ujian',$id);
        $masuk = $this->db->delete('ujian');
        if ($masuk) { $status = 'berhasil'; }
        $hasil = array('status'=>$status);
        echo json_encode($hasil);	
		
    }

    public function aktifkan_ujian()
    {
        $status = 'gagal';
        $idujian = $this->input->post('idujian');
        $ujian = $this->model_kueri->kueri_row_array('ujian',array('id_ujian'=>$idujian));
        $idkelas = $ujian->kelas_id;
        $jumlah_kolom = $ujian->jumlah_kolom;
        $durasi = $ujian->durasi_kolom;
        for ($i=1;$i<=$jumlah_kolom;$i++)
        {
            $peserta = $this->model_kueri->pilih_array('peserta',array('kelas_id'=>$idkelas));
            foreach ($peserta as $ps)
            {
                $data = array('peserta_id'=>$ps->peserta_id,'ujian_id'=>$idujian,'kolom_soal'=>$i);
                $this->db->insert('peserta_hasil',$data);
            }

        }
        $this->model_utama->update('ujian',array('aktif'=>'Y'),array('id_ujian'=>$idujian));
        $status = 'berhasil'; 
        $hasil = array('status'=>$status);
        echo json_encode($hasil);	
    }

    public function ujian_kolom()
	{
        $idujian = $this->uri->segment(3);
        $ujian = $this->model_kueri->kueri_row_array('ujian',array('id_ujian'=>$idujian));
        $data['judul'] = 'Pembuatan Kolom dan Soal : '.$ujian->nama_ujian;
        $data['idujian'] = $idujian;
        $data['jumkolom']= $ujian->jumlah_kolom;
        $this->template->display('v_ujian_kolom',$data);
    }

    public function ujian_kolom_koran()
    {
        $idujian = $this->uri->segment(3);
        $ujian = $this->model_kueri->kueri_row_array('ujian',array('id_ujian'=>$idujian));
        $data['judul'] = 'Pembuatan Soal : '.$ujian->nama_ujian;
        $data['idujian'] = $idujian;
        $data['jumkolom']= $ujian->jumlah_kolom;
        $this->template->display('v_ujian_kolom_koran',$data);
    }

    public function kolom_add()
	{
        $status = 'gagal';
        $idujian = $this->input->post('idujian');
        $kolom = $this->input->post('kolom');
        $huruf = $this->input->post('huruf');
        $nilai = $this->input->post('nilai');
        $cekada = $this->model_kueri->cek_jumlah_array_minimal('id_ujian_kolom','ujian_kolom',array('ujian_id'=>$idujian,'kolom'=>$kolom,'huruf'=>$huruf));
        if ($cekada < 1)
        {
            $data = array('ujian_id'=>$idujian,'kolom'=>$kolom,'huruf'=>$huruf,'kolom_nilai'=>$nilai);
            $masuk =$this->db->insert('ujian_kolom', $data);
            if ($masuk) { $status = 'berhasil'; }
        }        
        
        $hasil = array('status'=>$status);
        echo json_encode($hasil);		    
    }

    public function kolom_koran_add()
    {
        $status = 'gagal';
        $idujian    = $this->input->post('idujian');
        $kolom      = $this->input->post('kolom');
        $baris      = $this->input->post('baris');
        $inp_soal   = $this->input->post('inp_soal');
        $cekada = $this->model_kueri->cek_jumlah_array_minimal('id','koran_ujian',array('ujian_id'=>$idujian,'kolom'=>$kolom));
        if ($cekada < 1)
        {
            $soal = array();
            $jawaban = array();

            $soal[] = array(
                'ujian_id'  => $idujian,
                'kolom'     => $kolom,
                'baris'     => $baris,
                's1'        => $_POST['inp_soal'][0],
                's2'        => $_POST['inp_soal'][1],
                's3'        => $_POST['inp_soal'][2],
                's4'        => $_POST['inp_soal'][3],
                's5'        => $_POST['inp_soal'][4],
                's6'        => $_POST['inp_soal'][5],
                's7'        => $_POST['inp_soal'][6],
                's8'        => $_POST['inp_soal'][7],
                's9'        => $_POST['inp_soal'][8],
                's10'       => $_POST['inp_soal'][9],

                's11'       => $_POST['inp_soal'][10],
                's12'       => $_POST['inp_soal'][11],
                's13'       => $_POST['inp_soal'][12],
                's14'       => $_POST['inp_soal'][13],
                's15'       => $_POST['inp_soal'][14],
                's16'       => $_POST['inp_soal'][15],
                's17'       => $_POST['inp_soal'][16],
                's18'       => $_POST['inp_soal'][17],
                's19'       => $_POST['inp_soal'][18],
                's20'       => $_POST['inp_soal'][19]
            );
            // if($this->complete($inp_soal) == 'ok'){
                $masuk =$this->db->insert_batch('koran_ujian', $soal);
                if ($masuk) { 
                    $status = 'berhasil'; 

                    $jawaban[] = array(
                        'ujian_id'  => $idujian,
                        'kolom'     => $kolom,
                        'baris'     => $baris,
                        's1'        => '',
                        's2'        => $this->sumData($_POST['inp_soal'][0], $_POST['inp_soal'][1]),
                        's3'        => $this->sumData($_POST['inp_soal'][1], $_POST['inp_soal'][2]),
                        's4'        => $this->sumData($_POST['inp_soal'][2], $_POST['inp_soal'][3]),
                        's5'        => $this->sumData($_POST['inp_soal'][3], $_POST['inp_soal'][4]),
                        's6'        => $this->sumData($_POST['inp_soal'][4], $_POST['inp_soal'][5]),
                        's7'        => $this->sumData($_POST['inp_soal'][5], $_POST['inp_soal'][6]),
                        's8'        => $this->sumData($_POST['inp_soal'][6], $_POST['inp_soal'][7]),
                        's9'        => $this->sumData($_POST['inp_soal'][7], $_POST['inp_soal'][8]),
                        's10'       => $this->sumData($_POST['inp_soal'][8], $_POST['inp_soal'][9]),

                        's11'       => $this->sumData($_POST['inp_soal'][9], $_POST['inp_soal'][10]),
                        's12'       => $this->sumData($_POST['inp_soal'][10], $_POST['inp_soal'][11]),
                        's13'       => $this->sumData($_POST['inp_soal'][11], $_POST['inp_soal'][12]),
                        's14'       => $this->sumData($_POST['inp_soal'][12], $_POST['inp_soal'][13]),
                        's15'       => $this->sumData($_POST['inp_soal'][13], $_POST['inp_soal'][14]),
                        's16'       => $this->sumData($_POST['inp_soal'][14], $_POST['inp_soal'][15]),
                        's17'       => $this->sumData($_POST['inp_soal'][15], $_POST['inp_soal'][16]),
                        's18'       => $this->sumData($_POST['inp_soal'][16], $_POST['inp_soal'][17]),
                        's19'       => $this->sumData($_POST['inp_soal'][17], $_POST['inp_soal'][18]),
                        's20'       => $this->sumData($_POST['inp_soal'][18], $_POST['inp_soal'][19])
                    );
                    $this->db->insert_batch('koran_jawaban', $jawaban);
                }
            // }
        }        
        
        $hasil = array('status'=>$status);
        echo json_encode($hasil);           
    }

    function complete($inp_soal){
        if(!empty($_POST['inp_soal'][0]) && !empty($_POST['inp_soal'][1]) && !empty($_POST['inp_soal'][2]) && !empty($_POST['inp_soal'][3]) && !empty($_POST['inp_soal'][4]) && !empty($_POST['inp_soal'][5]) && !empty($_POST['inp_soal'][6]) && !empty($_POST['inp_soal'][7]) && !empty($_POST['inp_soal'][8]) && !empty($_POST['inp_soal'][9]) && !empty($_POST['inp_soal'][10]) && !empty($_POST['inp_soal'][11]) && !empty($_POST['inp_soal'][12]) && !empty($_POST['inp_soal'][13]) && !empty($_POST['inp_soal'][14]) && !empty($_POST['inp_soal'][15]) && !empty($_POST['inp_soal'][16]) && !empty($_POST['inp_soal'][17]) && !empty($_POST['inp_soal'][18]) && !empty($_POST['inp_soal'][19])){
            $stt = 'ok';
        }else{
            $stt = 'gg';
        }
        return $stt;
    }

    function sumData($first, $two){
        if(!empty($first) && !empty($two)){
            $jumlah = $first + $two;
            $pjg    = strlen($jumlah);
            $jumlah= substr($jumlah,-1,$pjg);
        }else{
            $jumlah = '';
        }

        return $jumlah;
    }

    public function hasil_kolom()
    {
        $idujian = $this->input->post('idujian');
        $data['kolom'] = $this->db->query("SELECT * FROM ujian_kolom WHERE ujian_id = '$idujian' GROUP BY kolom ORDER BY kolom ASC");
        $data['idujian'] = $idujian;
        $this->load->view('v_hasil_kolom',$data); 
 
    }

    public function hasil_kolom_koran()
    {
        $idujian = $this->input->post('idujian');
        $data['kolom'] = $this->db->query("SELECT * FROM koran_ujian WHERE ujian_id = '$idujian' GROUP BY kolom ORDER BY kolom ASC");
        $data['idujian'] = $idujian;
        $this->load->view('v_hasil_kolom_koran',$data); 
 
    }

    public function kolom_delete()
	{
        $status = 'gagal';
        $id = $this->input->post('idkolom');
        $masuk = $this->db->where('id_ujian_kolom',$id);
        $masuk = $this->db->delete('ujian_kolom');
        if ($masuk) { $status = 'berhasil'; }
        $hasil = array('status'=>$status);
        echo json_encode($hasil);	
		
    }

    public function kolom_koran_delete()
    {
        $status = 'gagal';
        $id = $this->input->post('idkolom');
        $masuk = $this->db->where('id',$id);
        $masuk = $this->db->delete('koran_ujian');
        if ($masuk) { $status = 'berhasil'; }
        $hasil = array('status'=>$status);
        echo json_encode($hasil);   
        
    }

    public function generate_soal()
    {
        $status = 'gagal';
        $idujian = $this->input->post('idujian');
        $jumlahsoal = $this->input->post('jumlahsoal');
        $kolom = $this->input->post('kolom');
        $huruf = array('A','B','C','D','E');
        $this->db->query("DELETE FROM ujian_soal WHERE ujian_id = '$idujian' AND kolom = '$kolom'");
        for ($i=1;$i<=$jumlahsoal;$i++)
        {
            shuffle($huruf);
            $acak = rand(0,4);
            $jawaban = $huruf[$acak];
            $data = array('ujian_id'=>$idujian,'kolom'=>$kolom,'soal_ke'=>$i,'jawaban'=>$jawaban);
            $datasoal = '';
            $soal = $this->db->query("SELECT kolom_nilai FROM ujian_kolom WHERE ujian_id = '$idujian' AND kolom = '$kolom' AND huruf != '$jawaban' ORDER BY RAND()")->result_array();
            foreach ($soal as $sl)
            {
                $datasoal .= $sl['kolom_nilai'].',';
            }
            $pecahsoal = explode(',',$datasoal);
            $data['huruf_1'] = $pecahsoal[0];
            $data['huruf_2'] = $pecahsoal[1];
            $data['huruf_3'] = $pecahsoal[2];
            $data['huruf_4'] = $pecahsoal[3];
            $insertsoal = $this->db->insert('ujian_soal',$data);
        }
        $status = 'berhasil';
        $hasil = array('status'=>$status);
        echo json_encode($hasil);	
    }

    public function lihat_soal($idujian,$kolom)
    {
       $data['ujikolom'] = $this->model_kueri->pilih_array_order('ujian_kolom',array('ujian_id'=>$idujian,'kolom'=>$kolom),'huruf','ASC');
       $data['ujisoal'] = $this->model_kueri->pilih_array_order('ujian_soal',array('ujian_id'=>$idujian,'kolom'=>$kolom),'soal_ke','ASC');
       $data['kolom'] = $kolom;
       $this->load->view('v_ujian_soal',$data);
      
    }

    public function ujian_detail()
    {
        $idujian = $this->uri->segment(3);
        $ujian = $this->db->query("SELECT * FROM ujian a JOIN kelas b ON a.kelas_id = b.id_kelas WHERE a.id_ujian = $idujian")->row();
        $data['judul'] = 'Detail Ujian : '.$ujian->nama_ujian.' Kelas : '.$ujian->nama_kelas;
        $data['idujian'] = $idujian;
        $data['jumlah_kolom'] = $ujian->jumlah_kolom;
        $data['jenis'] = $ujian->jenis;
        $this->template->display('v_ujian_detail',$data);
    }
   

    
    


}
