<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    function __construct(){
        parent::__construct();
		if (!$this->ion_auth->logged_in()) { redirect('auth/login', 'refresh'); }
		
    }
	
	public function index()
	{
		unset($_SESSION['soalke']);
		$data['judul'] = 'Welcome '.$this->ion_auth->user()->row()->first_name;
		if ($this->ion_auth->in_group(1))
		{
			$this->load->model('model_user');
			$data['jumpeserta'] = $this->model_user->count_all();
			$this->template->display('v_dashboard_admin',$data);

		}  else {
			$idpeserta = $this->ion_auth->user()->row()->id;
			$ujian = $this->db->query("SELECT a.id_ujian,a.tanggal,a.jenis FROM ujian a JOIN peserta_ujian b ON a.id_ujian = b.ujian_id WHERE b.peserta_id = '$idpeserta'")->row();
			$idujian = $ujian->id_ujian;
			$data['idujian'] = $idujian;
			$data['idpeserta'] = $idpeserta;
			$data['tanggalujian'] = $ujian->tanggal;
			if($ujian->jenis == 'ujian'){
				$data['home'] = $this->model_kueri->kueri_row_array('home',array('id_home'=>1));
			}else{
				$data['home'] = $this->model_kueri->kueri_row_array('home',array('id_home'=>2));
			}
			$data['ceksudah'] = $this->model_kueri->cek_jumlah_array_minimal('id_peserta_ujian','peserta_ujian',array('peserta_id'=>$idpeserta,'ujian_id'=>$idujian,'status'=>'Y'));
			$this->template->display('v_dashboard_peserta',$data);

		}
		
	}

	public function profil()
	{
		$data['judul'] = 'Profil';
		$data['poin'] = $this->model_kueri->return_kolom_array('nilai','pengaturan',array('id'=>1));
		$data['bantuan'] = $this->model_kueri->return_kolom_array('nilai','pengaturan',array('id'=>2));
		$this->template->display('v_profil',$data);
		
	}

	public function update()
	{
		$status = 'gagal';
		$username = $this->input->post('username');
		$nama = $this->input->post('nama');
		$phone = $this->input->post('phone');
		$password = $this->input->post('password');
		$poin = $this->input->post('poin');
		$bantuan = $this->input->post('bantuan');
		if ($this->ion_auth->in_group(1))
		{
			$datau = array(
				'username'=>$username,
				'first_name' => $nama,
				'phone'=>$phone
			);
			$ab = array(
				'nilai' => $poin
			);
			$cd = array(
				'nilai' => $poin
			);
			$this->db->where('id', 1);
			$this->db->update('pengaturan', $ab);
			$cd = array(
				'nilai' => $bantuan
			);
			$this->db->where('id', 2);
            $this->db->update('pengaturan', $cd);
		}
		if ($this->ion_auth->in_group(2))
		{
			$datau = array(
				'phone'=>$phone
				 );
		}
		
		if ($this->input->post('password')!='') { 
		$datau['password'] = $this->input->post('password');
		}
		$masuk = $this->ion_auth->update($this->ion_auth->user()->row()->id, $datau);
		$status = 'berhasil';
		$hasil = array('status'=>$status,'poin'=>$bantuan);
		echo json_encode($hasil);

	}

	public function bantuan()
	{
		$data['judul'] = 'Bantuan';
		$data['bantuan'] = $this->model_kueri->return_kolom_array('nilai','pengaturan',array('id'=>2));
		$this->template->display('v_bantuan',$data);
		
	}
}
