-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.18-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             12.5.0.6677
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for catpsi
CREATE DATABASE IF NOT EXISTS `catpsi` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `catpsi`;

-- Dumping structure for table catpsi.koran_jawaban
CREATE TABLE IF NOT EXISTS `koran_jawaban` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ujian_id` int(11) DEFAULT NULL,
  `kolom` int(11) DEFAULT NULL,
  `baris` int(11) DEFAULT NULL,
  `s1` varchar(10) DEFAULT NULL,
  `s2` varchar(10) DEFAULT NULL,
  `s3` varchar(10) DEFAULT NULL,
  `s4` varchar(10) DEFAULT NULL,
  `s5` varchar(10) DEFAULT NULL,
  `s6` varchar(10) DEFAULT NULL,
  `s7` varchar(10) DEFAULT NULL,
  `s8` varchar(10) DEFAULT NULL,
  `s9` varchar(10) DEFAULT NULL,
  `s10` varchar(10) DEFAULT NULL,
  `s11` varchar(10) DEFAULT NULL,
  `s12` varchar(10) DEFAULT NULL,
  `s13` varchar(10) DEFAULT NULL,
  `s14` varchar(10) DEFAULT NULL,
  `s15` varchar(10) DEFAULT NULL,
  `s16` varchar(10) DEFAULT NULL,
  `s17` varchar(10) DEFAULT NULL,
  `s18` varchar(10) DEFAULT NULL,
  `s19` varchar(10) DEFAULT NULL,
  `s20` varchar(10) DEFAULT NULL,
  `s21` varchar(10) DEFAULT NULL,
  `s22` varchar(10) DEFAULT NULL,
  `s23` varchar(10) DEFAULT NULL,
  `s24` varchar(10) DEFAULT NULL,
  `s25` varchar(10) DEFAULT NULL,
  `s26` varchar(10) DEFAULT NULL,
  `s27` varchar(10) DEFAULT NULL,
  `s28` varchar(10) DEFAULT NULL,
  `s29` varchar(10) DEFAULT NULL,
  `s30` varchar(10) DEFAULT NULL,
  `s31` varchar(10) DEFAULT NULL,
  `s32` varchar(10) DEFAULT NULL,
  `s33` varchar(10) DEFAULT NULL,
  `s34` varchar(10) DEFAULT NULL,
  `s35` varchar(10) DEFAULT NULL,
  `s36` varchar(10) DEFAULT NULL,
  `s37` varchar(10) DEFAULT NULL,
  `s38` varchar(10) DEFAULT NULL,
  `s39` varchar(10) DEFAULT NULL,
  `s40` varchar(10) DEFAULT NULL,
  `s41` varchar(10) DEFAULT NULL,
  `s42` varchar(10) DEFAULT NULL,
  `s43` varchar(10) DEFAULT NULL,
  `s44` varchar(10) DEFAULT NULL,
  `s45` varchar(10) DEFAULT NULL,
  `s46` varchar(10) DEFAULT NULL,
  `s47` varchar(10) DEFAULT NULL,
  `s48` varchar(10) DEFAULT NULL,
  `s49` varchar(10) DEFAULT NULL,
  `s50` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table catpsi.koran_jawaban: ~1 rows (approximately)
INSERT INTO `koran_jawaban` (`id`, `ujian_id`, `kolom`, `baris`, `s1`, `s2`, `s3`, `s4`, `s5`, `s6`, `s7`, `s8`, `s9`, `s10`, `s11`, `s12`, `s13`, `s14`, `s15`, `s16`, `s17`, `s18`, `s19`, `s20`, `s21`, `s22`, `s23`, `s24`, `s25`, `s26`, `s27`, `s28`, `s29`, `s30`, `s31`, `s32`, `s33`, `s34`, `s35`, `s36`, `s37`, `s38`, `s39`, `s40`, `s41`, `s42`, `s43`, `s44`, `s45`, `s46`, `s47`, `s48`, `s49`, `s50`) VALUES
	(1, 13, 1, 50, '', '3', '5', '7', '9', '12', '15', '17', '16', '15', '17', '10', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2');

-- Dumping structure for table catpsi.koran_ujian
CREATE TABLE IF NOT EXISTS `koran_ujian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ujian_id` int(11) DEFAULT NULL,
  `kolom` int(11) DEFAULT NULL,
  `baris` int(11) DEFAULT NULL,
  `s1` varchar(10) DEFAULT NULL,
  `s2` varchar(10) DEFAULT NULL,
  `s3` varchar(10) DEFAULT NULL,
  `s4` varchar(10) DEFAULT NULL,
  `s5` varchar(10) DEFAULT NULL,
  `s6` varchar(10) DEFAULT NULL,
  `s7` varchar(10) DEFAULT NULL,
  `s8` varchar(10) DEFAULT NULL,
  `s9` varchar(10) DEFAULT NULL,
  `s10` varchar(10) DEFAULT NULL,
  `s11` varchar(10) DEFAULT NULL,
  `s12` varchar(10) DEFAULT NULL,
  `s13` varchar(10) DEFAULT NULL,
  `s14` varchar(10) DEFAULT NULL,
  `s15` varchar(10) DEFAULT NULL,
  `s16` varchar(10) DEFAULT NULL,
  `s17` varchar(10) DEFAULT NULL,
  `s18` varchar(10) DEFAULT NULL,
  `s19` varchar(10) DEFAULT NULL,
  `s20` varchar(10) DEFAULT NULL,
  `s21` varchar(10) DEFAULT NULL,
  `s22` varchar(10) DEFAULT NULL,
  `s23` varchar(10) DEFAULT NULL,
  `s24` varchar(10) DEFAULT NULL,
  `s25` varchar(10) DEFAULT NULL,
  `s26` varchar(10) DEFAULT NULL,
  `s27` varchar(10) DEFAULT NULL,
  `s28` varchar(10) DEFAULT NULL,
  `s29` varchar(10) DEFAULT NULL,
  `s30` varchar(10) DEFAULT NULL,
  `s31` varchar(10) DEFAULT NULL,
  `s32` varchar(10) DEFAULT NULL,
  `s33` varchar(10) DEFAULT NULL,
  `s34` varchar(10) DEFAULT NULL,
  `s35` varchar(10) DEFAULT NULL,
  `s36` varchar(10) DEFAULT NULL,
  `s37` varchar(10) DEFAULT NULL,
  `s38` varchar(10) DEFAULT NULL,
  `s39` varchar(10) DEFAULT NULL,
  `s40` varchar(10) DEFAULT NULL,
  `s41` varchar(10) DEFAULT NULL,
  `s42` varchar(10) DEFAULT NULL,
  `s43` varchar(10) DEFAULT NULL,
  `s44` varchar(10) DEFAULT NULL,
  `s45` varchar(10) DEFAULT NULL,
  `s46` varchar(10) DEFAULT NULL,
  `s47` varchar(10) DEFAULT NULL,
  `s48` varchar(10) DEFAULT NULL,
  `s49` varchar(10) DEFAULT NULL,
  `s50` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table catpsi.koran_ujian: ~1 rows (approximately)
INSERT INTO `koran_ujian` (`id`, `ujian_id`, `kolom`, `baris`, `s1`, `s2`, `s3`, `s4`, `s5`, `s6`, `s7`, `s8`, `s9`, `s10`, `s11`, `s12`, `s13`, `s14`, `s15`, `s16`, `s17`, `s18`, `s19`, `s20`, `s21`, `s22`, `s23`, `s24`, `s25`, `s26`, `s27`, `s28`, `s29`, `s30`, `s31`, `s32`, `s33`, `s34`, `s35`, `s36`, `s37`, `s38`, `s39`, `s40`, `s41`, `s42`, `s43`, `s44`, `s45`, `s46`, `s47`, `s48`, `s49`, `s50`) VALUES
	(1, 13, 1, 50, '1', '2', '3', '4', '5', '7', '8', '9', '7', '8', '9', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
